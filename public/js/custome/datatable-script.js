if (typeof jQuery !== 'undefined' && typeof jQuery.fn.DataTable !== 'undefined') {
    $(document).ready(function(){
        const urlAjax = $('#table-datapeserta').data('link');
        const baseUrlElement = document.getElementById('base-url');
        const baseUrl        = baseUrlElement.getAttribute('data-baseurl');

        
        
        // $.ajaxSetup({
        //     headers: {
        //     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        //     }        
        // })
    
        // Datatable Peserta
        $('#table-datapengguna').DataTable({
            "language": {
            "lengthMenu": "Show _MENU_",
            },
            "dom":
            "<'row'" +
            "<'col-sm-6 d-flex align-items-center justify-conten-start'l>" +
            "<'col-sm-6 d-flex align-items-center justify-content-end'f>" +
            ">" +
        
            "<'table-responsive'tr>" +
        
            "<'row'" +
            "<'col-sm-12 col-md-5 d-flex align-items-center justify-content-center justify-content-md-start'i>" +
            "<'col-sm-12 col-md-7 d-flex align-items-center justify-content-center justify-content-md-end'p>" +
            ">",
            processing: true,
            serverSide: true,
            pageLength:  50,
            language: {
                processing: 'Sedang memuat data...',
                searchPlaceholder: "Cari data ..."
            },
            ajax: {
            url: baseUrl + '/pengguna-data',
            error: function (xhr, error, thrown) {
                toastNotif('error', 'Gagal Melakukan load data dari sistem, pastikan internet anda lancar', 'GAGAL LOAD DATA', )}
            },
            columns: [
            { data: 'nama_lengkap', name: 'nama_lengkap'},
            { data: 'level_akun', name: 'level_akun', 'className': 'text-center', orderable: false, searchable: false },
            { data: 'status_akun', name: 'status_akun', 'className': 'text-center', orderable: false, searchable: false },
            { data: 'email', name: 'email', 'className': 'text-left'},
            { data: 'aksi', name: 'aksi', orderable: false, searchable: false , 'className': 'text-center'},
            ],
            createdRow: function (row, data, dataIndex) {
                $(row).addClass('detail-peserta');
                $(row).attr('data-idpeserta', data.peserta_id);
            },
        });

        $('#table-manajemen-kegiatan').DataTable({
            "language": {
            "lengthMenu": "Show _MENU_",
            },
            "dom":
            "<'row'" +
            "<'col-sm-6 d-flex align-items-center justify-conten-start'l>" +
            "<'col-sm-6 d-flex align-items-center justify-content-end'f>" +
            ">" +
        
            "<'table-responsive'tr>" +
        
            "<'row'" +
            "<'col-sm-12 col-md-5 d-flex align-items-center justify-content-center justify-content-md-start'i>" +
            "<'col-sm-12 col-md-7 d-flex align-items-center justify-content-center justify-content-md-end'p>" +
            ">",
            processing: true,
            serverSide: true,
            pageLength:  50,
            language: {
                processing: 'Sedang memuat data...',
                searchPlaceholder: "Cari data ..."
            },
            ajax: {
                url: baseUrl + '/data-manajemen-kegiatan',
                error: function (xhr, error, thrown) {
                toastNotif('error', 'Gagal Melakukan load data dari sistem, pastikan internet anda lancar', 'GAGAL LOAD DATA', )}
            },
            columns: [
                // { data: 'DT_RowIndex', name: '#', 'className': 'text-center'},
                { data: 'nama_kegiatan', name: 'nama_kegiatan', 'className': 'text-left'},
                { data: 'maximal_anggota', name: 'Jumlah Pendaftar', 'className': 'text-center'},
                { data: 'waktu_kegiatan', name: 'Waktu Kegiatan', 'className': 'text-center'},
                { data: 'waktu_pendaftaran_ditutup', name: 'Waktu Ditutup', 'className': 'text-center'},
                { data: 'status_kegiatan', name: 'Status', 'className': 'text-center'},
                { data: 'aksi', name: 'Aksi', 'className': 'text-center'},

            ],
            // createdRow: function (row, data, dataIndex) {
            //     $(row).addClass('detail-peserta');
            //     $(row).attr('data-idpeserta', data.peserta_id);
            // },
        });

        $('#table-dataruangan').DataTable({
            "language": {
            "lengthMenu": "Show _MENU_",
            },
            "dom":
            "<'row'" +
            "<'col-sm-6 d-flex align-items-center justify-conten-start'l>" +
            "<'col-sm-6 d-flex align-items-center justify-content-end'f>" +
            ">" +
        
            "<'table-responsive'tr>" +
        
            "<'row'" +
            "<'col-sm-12 col-md-5 d-flex align-items-center justify-content-center justify-content-md-start'i>" +
            "<'col-sm-12 col-md-7 d-flex align-items-center justify-content-center justify-content-md-end'p>" +
            ">",
            processing: true,
            serverSide: true,
            pageLength:  50,
            language: {
                processing: 'Sedang memuat data...',
                searchPlaceholder: "Cari data ..."
            },
            ajax: {
            url: baseUrl + '/ruangan-data',
            error: function (xhr, error, thrown) {
                toastNotif('error', 'Gagal Melakukan load data dari sistem, pastikan internet anda lancar', 'GAGAL LOAD DATA', )}
            },
            columns: [
                { data: 'nama_ruangan', name: 'nama_ruangan'},
                { data: 'status_ruangan', name: 'status_ruangan', 'className': 'text-center', orderable: false, searchable: false },
                { data: 'jumlah_kursi', name: 'jumlah_kursi', 'className': 'text-center'},
                { data: 'warna_ruangan', name: 'warna_ruangan', 'className': 'text-center'},
                { data: 'keterangan', name: 'keterangan', 'className': 'text-left'},
                { data: 'aksi', name: 'aksi', orderable: false, searchable: false , 'className': 'text-center'},
            ]
        });

        $('#table-bookingroom').DataTable({
            "language": {
            "lengthMenu": "Show _MENU_",
            },
            "dom":
            "<'row'" +
            "<'col-sm-6 d-flex align-items-center justify-conten-start'l>" +
            "<'col-sm-6 d-flex align-items-center justify-content-end'f>" +
            ">" +
        
            "<'table-responsive'tr>" +
        
            "<'row'" +
            "<'col-sm-12 col-md-5 d-flex align-items-center justify-content-center justify-content-md-start'i>" +
            "<'col-sm-12 col-md-7 d-flex align-items-center justify-content-center justify-content-md-end'p>" +
            ">",
            processing: true,
            serverSide: true,
            pageLength:  50,
            language: {
                processing: 'Sedang memuat data...',
                searchPlaceholder: "Cari data ..."
            },
            ajax: {
            url: baseUrl + '/bookingroom-data',
            error: function (xhr, error, thrown) {
                toastNotif('error', 'Gagal Melakukan load data dari sistem, pastikan internet anda lancar', 'GAGAL LOAD DATA', )}
            },
            columns: [
                { data: 'nama_ruangan', name: 'nama_ruangan', orderable: true},
                { data: 'created_at', name: 'created_at'},
                { data: 'rentang_tanggal', name: 'rentang_tanggal', 'className': 'text-center', orderable: true},
                { data: 'rentang_waktu', name: 'rentang_waktu', 'className': 'text-center', orderable: true},
                { data: 'tujuan_kegiatanbook', name: 'tujuan_kegiatanbook', orderable: true},                
                { data: 'dibooking', name: 'dibooking', 'className': 'text-left', orderable: true},
                { data: 'aksi', name: 'aksi', orderable: true, searchable: false , 'className': 'text-center'},
            ],
            order: [[1, 'desc']],
            columnDefs: [
                {
                    targets: [1], // Indeks kolom yang akan digunakan untuk pengurutan
                    visible: false, // Kolom tidak akan ditampilkan
                    searchable: false // Kolom tidak akan digunakan untuk pencarian
                }]

        });

        
        

        // if (document.getElementById('catatan_kegiatan')) {
        //     const laporanKegiatan = document.getElementById('laporan-kegiatanid');
        //     const laporanId       = laporanKegiatan.getAttribute('data-idkeg');
            
        //     $('#table-anggota-kegiatan').DataTable({
        //         "language": {
        //                         "lengthMenu": "Show _MENU_",
        //                     },
        //         "dom": "<'row'" + "<'col-sm-6 d-flex align-items-center justify-conten-start'l>" + "<'col-sm-6 d-flex align-items-center justify-content-end'f>" + ">" +             "<'table-responsive'tr>" +             "<'row'" + "<'col-sm-12 col-md-5 d-flex align-items-center justify-content-center justify-content-md-start'i>" + "<'col-sm-12 col-md-7 d-flex align-items-center justify-content-center justify-content-md-end'p>" + ">",
        //         processing: true,
        //         serverSide: true,
        //         pageLength:  50,
        //         language: {
        //             processing: 'Sedang memuat data...',
        //             searchPlaceholder: "Cari data ..."
        //         },
        //         ajax: {
        //                 url: baseUrl + '/anggota-kegiatan',
        //                 data: {laporan: laporanId},
        //                 error: function (xhr, error, thrown) {
        //                     toastNotif('error', 'Gagal Melakukan load data dari sistem, pastikan internet anda lancar', 'GAGAL LOAD DATA', )}
        //             },
        //         columns: [
        //             { data: 'DT_RowIndex', name: '#', 'className': 'text-center'},
        //             { data: 'jabatan', name: 'Jabatan'},
        //             { data: 'nama_anggota', name: 'Nama Lengkap'},
        //             { data: 'keterangan', name: 'Keterangan'},
        //         ],
        //         "columnDefs": [
        //         {"width": "5%", "targets": 0 }
        //         ],
        //     });
    
        //     $('#table-anggota-kegiatan-modal').DataTable({
        //         "language": {
        //                         "lengthMenu": "Show _MENU_",
        //                     },
        //         "dom": "<'row'" + "<'col-sm-6 d-flex align-items-center justify-conten-start'l>" + "<'col-sm-6 d-flex align-items-center justify-content-end'f>" + ">" +             "<'table-responsive'tr>" +             "<'row'" + "<'col-sm-12 col-md-5 d-flex align-items-center justify-content-center justify-content-md-start'i>" + "<'col-sm-12 col-md-7 d-flex align-items-center justify-content-center justify-content-md-end'p>" + ">",
        //         processing: true,
        //         serverSide: true,
        //         pageLength:  50,
        //         language: {
        //             processing: 'Sedang memuat data...',
        //             searchPlaceholder: "Cari data ..."
        //         },
        //         ajax: {
        //                 url: baseUrl + '/anggota-kegiatan',
        //                 data: {laporan: laporanId},
        //                 error: function (xhr, error, thrown) {
        //                         toastNotif('error', 'Gagal Melakukan load data dari sistem, pastikan internet anda lancar', 'GAGAL LOAD DATA', )}
        //             },
        //         columns: [
        //             { data: 'DT_RowIndex', name: '#', 'className': 'text-center'},
        //             { data: 'jabatan', name: 'Jabatan'},
        //             { data: 'nama_anggota', name: 'Nama Lengkap'},
        //             { data: 'keterangan', name: 'Keterangan'},
        //             { data: 'aksi', name: 'Aksi', orderable: false, searchable: false , 'className': 'text-center'},
        //         ],
        //         "columnDefs": [
        //         {"width": "5%", "targets": 0 }
        //         ],
        //     });

        //     $('#table-filelampiran-modal').DataTable({
        //         "language": {
        //                         "lengthMenu": "Show _MENU_",
        //                     },
        //         "dom": "<'row'" + "<'col-sm-6 d-flex align-items-center justify-conten-start'l>" + "<'col-sm-6 d-flex align-items-center justify-content-end'f>" + ">" +             "<'table-responsive'tr>" +             "<'row'" + "<'col-sm-12 col-md-5 d-flex align-items-center justify-content-center justify-content-md-start'i>" + "<'col-sm-12 col-md-7 d-flex align-items-center justify-content-center justify-content-md-end'p>" + ">",
        //         processing: true,
        //         serverSide: true,
        //         pageLength:  50,
        //         language: {
        //             processing: 'Sedang memuat data...',
        //             searchPlaceholder: "Cari data ..."
        //         },
        //         ajax: {
        //                 url: baseUrl + '/table-lampiran-kegiatan',
        //                 data: {laporan: laporanId},
        //                 error: function (xhr, error, thrown) {
        //                         toastNotif('error', 'Gagal Melakukan load data dari sistem, pastikan internet anda lancar', 'GAGAL LOAD DATA', )}
        //             },
        //         columns: [
        //             { data: 'DT_RowIndex', name: '#', 'className': 'text-center align-middle'},
        //             { data: 'nama_lampiran', name: 'Nama File', 'className': 'align-middle'},
        //             { data: 'aksi', name: 'Aksi', orderable: false, searchable: false , 'className': 'text-center align-middle'},
        //         ],
        //         "columnDefs": [
        //         {"width": "5%", "targets": 0 },
        //         {"width": "5%", "targets": 2 },
        //         ],
        //     });

        //     $('#table-filedokumentasi-modal').DataTable({
        //         "language": {
        //                         "lengthMenu": "Show _MENU_",
        //                     },
        //         "dom": "<'row'" + "<'col-sm-6 d-flex align-items-center justify-conten-start'l>" + "<'col-sm-6 d-flex align-items-center justify-content-end'f>" + ">" +             "<'table-responsive'tr>" +             "<'row'" + "<'col-sm-12 col-md-5 d-flex align-items-center justify-content-center justify-content-md-start'i>" + "<'col-sm-12 col-md-7 d-flex align-items-center justify-content-center justify-content-md-end'p>" + ">",
        //         processing: true,
        //         serverSide: true,
        //         pageLength:  50,
        //         language: {
        //             processing: 'Sedang memuat data...',
        //             searchPlaceholder: "Cari data ..."
        //         },
        //         ajax: {
        //                 url: baseUrl + '/table-dokumentasi-kegiatan',
        //                 data: {laporan: laporanId},
        //                 error: function (xhr, error, thrown) {
        //                         toastNotif('error', 'Gagal Melakukan load data dari sistem, pastikan internet anda lancar', 'GAGAL LOAD DATA', )}
        //             },
        //         columns: [
        //             { data: 'DT_RowIndex', name: '#', 'className': 'text-center align-middle'},
        //             { data: 'nama_dokumentasi', name: 'File', 'className': 'align-middle'},
        //             { data: 'keterangan', name: 'Keterangan', 'className': 'align-middle'},
        //             { data: 'aksi', name: 'Aksi', orderable: false, searchable: false , 'className': 'text-center align-middle'},
        //         ],
        //         "columnDefs": [
        //         {"width": "5%", "targets": 0 },
        //         {"width": "5%", "targets": 3 },
        //         ],
        //     });
        // }

    });
}

// $(document).on('shown.bs.modal', function (e) {
    
//     var modalId = $(e.target).attr('id');
//     var idDataTable;

//     if (modalId === 'modalEditAnggota') {            
//         var idDataTable = '#table-anggota-kegiatan-modal';
//     }
//     else if (modalId === 'modalEditLampiran') {
//         var idDataTable = '#table-filelampiran-modal';
//     }
//     else if (modalId === 'modalEditDokumentasi') {
//         var idDataTable = '#table-filedokumentasi-modal';
//     }

//     if (idDataTable) {
//         var dataTable = $(idDataTable).DataTable();
//         dataTable.columns.adjust().draw();            
//     }
    
// });
