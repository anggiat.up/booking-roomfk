
document.addEventListener('DOMContentLoaded', function() {

    moment.locale('id');
    let calendar;
    if (document.getElementById('calendar-booking')) {
        var calendarEl = document.getElementById('calendar-booking');

        var eventList;
        const baseUrlElement = document.getElementById('base-url');
        const baseUrl        = baseUrlElement.getAttribute('data-baseurl');

        // Function to filter events based on selected rooms
        
        $.ajax({
            url: baseUrl + '/data-booked-calendar',
            type: 'GET',
            dataType: 'json',
            success: function (data) {
                // Operasi atau tindakan setelah mendapatkan respons
                eventList = data;

                // Panggil fungsi atau lakukan sesuatu dengan eventList di sini
                calendar = new Calendar(calendarEl, {
                    locale      : 'id',
                    plugins     : [dayGridPlugin, interactionPlugin, listPlugin, timegridPlugin],
                    selectable: true,
                    initialView : 'dayGridMonth', // Set tampilan bulan sebagai tampilan awal
                    aspectRatio : 1.5, // Ubah rasio aspek kalender sesuai kebutuhan
                    height: 900,
                    // contentHeight: 'auto',
                    headerToolbar: {
                        start: 'prev,next, title',
                        end: 'dayGridMonth'
                    },
                    buttonText: {
                        today: 'Hari Ini',
                        month: 'Bulan',
                    },
                    eventDisplay: 'block',
                    eventContent: function(arg) {
                        const { event }   = arg;
                        const content     = document.createElement('div');
                        const screenWidth = window.innerWidth;
                        const laptopWidth = 1366;

                         // Mendapatkan jumlah hari dari event
                        const startDate      = new Date(event.start);
                        const endDate        = new Date(event.end);
                        const daysDifference = Math.ceil((endDate - startDate) / (1000 * 60 * 60 * 24));
                        const textLimit      = 12 * daysDifference;


                        // Cek apakah lebar layar lebih kecil dari ukuran laptop
                        if (screenWidth <= laptopWidth) 
                        {
                            content.innerHTML = `
                                                    <div class="fc-title" style="color:${arg.textColor};"> ${arg.timeText} </div>
                                                `;
                        } else {
                            content.innerHTML = `
                                                    <div class="fc-title" style="color:${arg.textColor};">${arg.timeText} | ${limitText(event.title, textLimit)}</div>                                                    
                                                `;
                        }

                        
                        return { domNodes: [content] };
                    },
                    eventDidMount: function(info) {
                        // Mengambil elemen tanggal

                        const screenWidth = window.innerWidth;
                        const laptopWidth = 1366;

                        // Cek apakah lebar layar lebih kecil dari ukuran laptop
                        
                        let dayCell = info.el.closest('.fc-daygrid-day');
                    
                        
                        // Tentukan logika untuk menambah tinggi berdasarkan jumlah event atau ukuran tertentu
                        if (info.event.extendedProps.events && info.event.extendedProps.events.length > 1) {
                            dayCell.style.height = "auto"; // Membuatnya dinamis jika ada lebih dari satu event
                        } 
                        else {
                            
                            if (screenWidth <= laptopWidth) 
                            {
                                dayCell.style.height = "auto"; // Default height
                            } 
                            else {
                                dayCell.style.height = "60px"; // Default height
                            }
                        }
                        dayCell.style.paddingBottom = "20px"; 

                        // dayCell.style.marginBottom = "100px"; 
                    },
                    

                    events: eventList,

            
                    dateClick: function(info) {
        
                        var days = document.querySelectorAll('.fc-daygrid-day');
                            days.forEach(function(day) {
                            day.classList.remove('bg-label-primary');
                        });
                        // change the day's background color just for fun
                        if (info.dayEl.classList.contains('bg-label-primary')) {
                            info.dayEl.classList.remove('bg-label-primary');
                        } else {
                            info.dayEl.classList.add('bg-label-primary');
                        }
                    },
                    select: function(info) {

                        var today   = new Date();
                        var endDate = new Date(info.endStr);
                        
                        endDate.setDate(endDate.getDate() - 1);
                        var endStrMinusOneDay = endDate.toISOString().split('T')[0];

                        if (endDate >= today) {

                            if (document.getElementById('flatpickr-range')) 
                            {                                
                                $('#flatpickr-range').flatpickr({
                                    defaultDate: [info.startStr, endStrMinusOneDay],
                                    minDate: "today",
                                    mode: "range",
                                    locale: {
                                        firstDayOfWeek: 1, // start week on Monday
                                        rangeSeparator: ' sampai '
                                    }
                                }).setDate([info.startStr, endStrMinusOneDay]);
                            }
                        }
                        else {
                            if (document.getElementById('flatpickr-range')) {
                            const $flatpickr = $("#flatpickr-range").flatpickr({
                                defaultDate: [info.startStr, endStrMinusOneDay],
                                minDate: "today",
                                mode: "range",
                                "locale": {
                                    firstDayOfWeek: 1, // start week on Monday
                                    rangeSeparator: ' sampai '
                                }
                            });
                            
                                $flatpickr.clear();
                            }
                            
                        }


                        var bookingRoomModal = new bootstrap.Modal(document.getElementById('bookingRoomModal'));
                        bookingRoomModal.show();
                        info.jsEvent.target.classList.add('bg-label-primary');
                    },
                    eventTimeFormat: { // Format waktu 24 jam
                        hour: '2-digit',
                        minute: '2-digit',
                        meridiem: false,
                        hour12: false
                    },
                    eventClick: function(info) {
                        // Display event details in modal
                        document.getElementById('nama-kegiatan').textContent    = info.event.title;
                        document.getElementById('waktu').textContent            = moment(info.event.start).format('HH:mm') + ' sd ' + moment(info.event.end).format('HH:mm') + ' WIB';
                        document.getElementById('lokasi').textContent           = info.event.extendedProps.location;
                        document.getElementById('tanggal').textContent          = info.event.extendedProps.tanggal;
                        document.getElementById('deskripsi').innerHTML          = info.event.extendedProps.catatan;
                        document.getElementById('dibookingoleh').textContent    = info.event.extendedProps.booking;
            
                        // Show the modal
                        var eventDetailsBookModal = new bootstrap.Modal(document.getElementById('eventDetailsBookModal'));
                        eventDetailsBookModal.show();
                    }                            
                    
                });
                calendar.render();

                // Initialize Flatpickr as inline calendar
                const inlineCalendar = document.getElementById('inline-calendar-book');
                let inlineCalInstance;
                if (inlineCalendar) {
                    inlineCalInstance = flatpickr(inlineCalendar, {
                        monthSelectorType: 'static',
                        inline: true,            
                    });
                }
                
                inlineCalInstance.config.onChange.push(function (date) {
                    calendar.changeView(calendar.view.type, moment(date[0]).format('YYYY-MM-DD'));
                });

                // Function to update Flatpickr calendar
                function updateInlineCalendar(date) {
                    if (inlineCalInstance) {
                        inlineCalInstance.setDate(date, true);
                    }
                }

                // Event handler for FullCalendar navigation
                calendar.on('dateClick', function(info) {
                    if (info.view.type === 'dayGridMonth') {
                        calendar.gotoDate(info.date);
                        updateInlineCalendar(info.date);
                    }
                });

                // Event handler for FullCalendar navigation using buttons
                calendar.on('prev', function(info) {
                    const newDate = calendar.getDate();
                    updateInlineCalendar(newDate);
                });

                calendar.on('next', function(info) {
                    const newDate = calendar.getDate();
                    updateInlineCalendar(newDate);
                });


                const initialDate = calendar.getDate();
                inlineCalInstance.setDate(initialDate, true);  
                
                // Add event listener to checkboxes
                document.querySelectorAll('input[name="data-ruangan"], #selectAllRuangan').forEach(function(checkbox) {
                    checkbox.addEventListener('change', function() {
                        if (this.id === 'selectAllRuangan') {
                            document.querySelectorAll('input[name="data-ruangan"]').forEach(function(roomCheckbox) {
                                roomCheckbox.checked = checkbox.checked;
                            });
                        }
                        filterEvents();
                    });
                });

                // Initial filter
                filterEvents();

                function filterEvents() {
                    let selectedRooms = [];
                    document.querySelectorAll('input[name="data-ruangan"]:checked').forEach(function(checkbox) {
                        selectedRooms.push(checkbox.value);
                    });
                
                    let filteredEvents = eventList.filter(function(event) {
                        return selectedRooms.includes(event.location);
                    });
                
                    calendar.removeAllEvents();
                    calendar.addEventSource(filteredEvents);
                }
            },
            error: function (xhr, status, error) {
                // Handle error
                console.error(error);
            }
        });


        

        

        $("#bookingRoomForm").submit(function (event) {
            event.preventDefault();
        
            const baseUrlElement = document.getElementById('base-url');
            const baseUrl        = baseUrlElement.getAttribute('data-baseurl');
            
            Swal.fire({
                title: '<h4>Booking Ruangan</h4>',
                icon: 'question',
                html: "Konfirmasi anda ingin melakukan booking room ?",
                customClass: { 
                    confirmButton: 'btn btn-primary',
                    cancelButton: 'btn btn-danger',
                },
                showCancelButton: true,
                confirmButtonText: 'Ya, booking'
            }).then((result) => {
                if (result.isConfirmed) 
                {
                    var formData = new FormData(this);
                    $.ajax({
                        url: baseUrl + '/booking-ruangan',
                        type: "post",
                        data: formData,
                        dataType: 'json',
                        contentType: false,
                        cache: false,
                        processData: false,
                        success: function (response) {
                            
                            if (response.statuslog == 'error') {
                                toastNotif('warning', response.message, 'GAGAL', );
                            } 
                            else if (response.statuslog == 'success') {
                                toastNotif(response.statuslog, response.message, response.title, baseUrl);
                                $('#bookingRoomForm')[0].reset();
                                $('#nama_ruangan').val(null).trigger('change');
                                $('#bookingRoomModal').modal('hide');
                                // calendar.refetchEvents();
                            }
                            
                        },
                        error: function (xhr) {
                            toastNotif('error', 'Terjadi kesalahan saat melakukan permintaan ke server', 'ERROR', );        
                        }
                    });
            
                }
                else{
                    toastNotif('info', 'Tindakan dibatalkan', 'INFO');
                }
            });
        });
    }

    function limitText(text, limit) {
        // Cek apakah panjang teks melebihi batas
        if (text.length > limit) {
            // Potong teks dan tambahkan ellipsis
            return text.substring(0, limit) + '...';
        } else {
            // Kembalikan teks asli jika tidak melebihi batas
            return text;
        }
    }

   
    
});

