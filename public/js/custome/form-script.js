    $("#form-login").submit(function (event) {
        event.preventDefault();

        const baseUrlElement = document.getElementById('base-url');
        const baseUrl        = baseUrlElement.getAttribute('data-baseurl');

        
        var formData = new FormData(this);
        $.ajax({
            url: baseUrl + '/login-pengguna',
            type: "post",
            data: formData,
            dataType: 'json',
            contentType: false,
            cache: false,
            processData: false,
            success: function (response) {
                toastNotif(response.statuslog, response.message, response.title, response.links);
            },
            error: function (xhr) {
                var responseData = JSON.parse(xhr.responseText);
                var message      = responseData.message;
                toastNotif('error', message, 'GAGAL');
            }
        });

    });

    $("#update-datapengguna").submit(function (event) {
        event.preventDefault();
    
        const baseUrlElement = document.getElementById('base-url');
        const baseUrl        = baseUrlElement.getAttribute('data-baseurl');
        
        Swal.fire({
            title: 'Update Data Pengguna',
            icon: 'question',
            text: "Apakah anda yakin ingin mengupdate pengguna ini ?",
            customClass: { 
                confirmButton: 'btn btn-primary',
                cancelButton: 'btn btn-danger',
            },
            showCancelButton: true,
            confirmButtonText: 'Ya, konfirmasi'
        }).then((result) => {
            if (result.isConfirmed) 
            {
                var formData = new FormData(this);
                $.ajax({
                    url: baseUrl + '/edit-pengguna',
                    type: "post",
                    data: formData,
                    dataType: 'json',
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function (response) {

                        if (response.statuslog == 'error') {
                            toastNotif('warning', response.message, 'GAGAL', );
                        } 
                        else {
                            $('#modalEditPengguna').modal('hide');
                            $('#update-datapengguna')[0].reset();
                    
                            toastNotif(response.statuslog, response.message, response.title);
                            $('#table-datapengguna').DataTable().ajax.reload();
                        }
                    },
                    error: function (xhr) {
                        toastNotif('error', 'Terjadi kesalahan saat melakukan permintaan ke server', 'ERROR', );             
                    }
                });
        
            }
            else{
                toastNotif('info', 'Tindakan dibatalkan', 'INFO');
            }
        });
    });

    $("#tambah-ruangan").submit(function (event) {
        event.preventDefault();
    
        const baseUrlElement = document.getElementById('base-url');
        const baseUrl        = baseUrlElement.getAttribute('data-baseurl');
        
        Swal.fire({
            title: '<h4>Tambah Ruangan</h4>',
            icon: 'question',
            html: "Konfirmasi tambah ruangan baru ?",
            customClass: { 
                confirmButton: 'btn btn-primary',
                cancelButton: 'btn btn-danger',
            },
            showCancelButton: true,
            confirmButtonText: 'Ya, tambah'
        }).then((result) => {
            if (result.isConfirmed) 
            {
                var formData = new FormData(this);
                $.ajax({
                    url: baseUrl + '/tambah-ruangan',
                    type: "post",
                    data: formData,
                    dataType: 'json',
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function (response) {
                        
                        if (response.statuslog == 'error') {
                            toastNotif('warning', response.message, 'GAGAL', );
                        } 
                        else {
                            toastNotif(response.statuslog, response.message, response.title);
                            $('#table-dataruangan').DataTable().ajax.reload();
                            $('#tambah-ruangan')[0].reset();
                            $('#modalTambahRuangan').modal('hide');
                        }
                        
                    },
                    error: function (xhr) {
                        toastNotif('error', 'Terjadi kesalahan saat melakukan permintaan ke server', 'ERROR', );        
                    }
                });
        
            }
            else{
                toastNotif('info', 'Tindakan dibatalkan', 'INFO');
            }
        });
    });

    $("#bookingRoomFormEdit").submit(function (event) {
        event.preventDefault();
    
        const baseUrlElement = document.getElementById('base-url');
        const baseUrl        = baseUrlElement.getAttribute('data-baseurl');
        
        Swal.fire({
            title: '<h4>Booking Ruangan</h4>',
            icon: 'question',
            html: "Konfirmasi anda ingin mengedit data booking room ?",
            customClass: { 
                confirmButton: 'btn btn-primary',
                cancelButton: 'btn btn-danger',
            },
            showCancelButton: true,
            confirmButtonText: 'Ya, edit'
        }).then((result) => {
            if (result.isConfirmed) 
            {
                var formData = new FormData(this);
                $.ajax({
                    url: baseUrl + '/booking-ruanganedit',
                    type: "post",
                    data: formData,
                    dataType: 'json',
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function (response) {
                        
                        if (response.statuslog == 'error') {
                            toastNotif('warning', response.message, 'GAGAL', );
                        } 
                        else if (response.statuslog == 'success') {
                            $('#table-bookingroom').DataTable().ajax.reload();
                            toastNotif(response.statuslog, response.message, response.title);
                            $('#bookingRoomFormEdit')[0].reset();
                            $('#nama_ruangan').val(null).trigger('change');
                            $('#modalEditBooking').modal('hide');
                        }
                        
                    },
                    error: function (xhr) {
                        toastNotif('error', 'Terjadi kesalahan saat melakukan permintaan ke server', 'ERROR', );        
                    }
                });
        
            }
            else{
                toastNotif('info', 'Tindakan dibatalkan', 'INFO');
            }
        });
    });