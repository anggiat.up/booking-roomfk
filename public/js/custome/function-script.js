    // Function Notifikasi Sistem
    function toastNotif(status, pesan, judul, urlPindahHalaman) {
        if (status !== '' && pesan !== '' && judul !== '') {
            toastr.options = {
            "closeButton"      : true,
            "debug"            : false,
            "newestOnTop"      : true,
            "progressBar"      : true,
            "positionClass"    : "toast-top-right",
            "preventDuplicates": false,
            "showDuration"     : "300",
            "hideDuration"     : "1000",
            "timeOut"          : "2000",
            "extendedTimeOut"  : "500",
            "showEasing"       : "swing",
            "hideEasing"       : "linear",
            "showMethod"       : "fadeIn",
            "hideMethod"       : "fadeOut",
            "onHidden"         : function() {
                if (urlPindahHalaman && urlPindahHalaman.trim() !== '') {
                // Pindah halaman setelah pesan toastr ditutup
                    window.location.href = urlPindahHalaman;
                }
            }
            };

            toastr[status](pesan, judul);
        }
    }

    // Script Logout User
    $('#logout-btn').click(function(){
        const link = $('#logout-btn').data('link');
            Swal.fire({
                  title: 'Logout',
                  icon: 'question',
                  text: "Apakah anda ingin logout dari aplikasi?",
                  type: 'question',
                  customClass: { 
                        confirmButton: 'btn btn-primary',
                        cancelButton: 'btn btn-danger',
                  },
                  showCancelButton: true,
                  confirmButtonText: 'Ya, logout sistem'
            }).then((result) => {
              if (result.isConfirmed) {
                window.location = link;
              }
            })
    });

    const input = document.getElementById('time-input');
    const minusBtn = document.getElementById('minus-btn');
    const plusBtn = document.getElementById('plus-btn');

    if (input && minusBtn && plusBtn) {
        minusBtn.addEventListener('click', function() {
            let currentValue = parseInt(input.value, 10);
            if (currentValue > 0) {
                input.value = currentValue - 15;
            }
        });

        plusBtn.addEventListener('click', function() {
            let currentValue = parseInt(input.value, 10);
            if (currentValue < 480) {
                input.value = currentValue + 15;
            }
        });
    }
    
    function detailPengguna(penggunaId)
    {
        const baseUrlElement = document.getElementById('base-url');
        const baseUrl = baseUrlElement.getAttribute('data-baseurl');

        $.ajax({
            url: baseUrl + '/pengguna-detail',
            type: 'GET',
            dataType:"json",
            data: { pengguna: penggunaId },
            success: function (data) 
            {
            document.getElementById('nama_lengkap').innerHTML    = data.nama_lengkap; 
            document.getElementById('email_pengguna').innerHTML  = data.email; 
            document.getElementById('no_hp').innerHTML           = data.nomor_hp; 
            document.getElementById('level_akun').innerHTML      = data.level_akun; 
            document.getElementById('status_akun').innerHTML     = data.status_akun; 
            
            $('#modalDetailpengguna').modal('show');
            },
            error: function (xhr) {
            if (xhr.status === 404) {
                var responseData = JSON.parse(xhr.responseText);
                var message = responseData.message;
                toastNotif('error', 'Tidak dapat menemukan data yang anda pilih', 'GAGAL', );
            }
            }
        });

    }

    function updatePengguna(penggunaId)
    {
        const baseUrlElement = document.getElementById('base-url');
        const baseUrl = baseUrlElement.getAttribute('data-baseurl');

        $.ajax({
            url: baseUrl + '/pengguna-detail',
            type: 'GET',
            dataType:"json",
            data: { pengguna: penggunaId },
            success: function (data) 
            {
                if (data.statuslog == 'error') {
                    toastNotif('warning', data.message, 'GAGAL', );
                } 
                else {
                    document.getElementById('pengguna_id').value       = data.pgnid; 
                    document.getElementById('editnama_lengkap').value  = data.nama_lengkap; 
                    document.getElementById('editemail').value         = data.email; 
                    document.getElementById('editno_hp').value         = data.nomor_hp; 
                    document.getElementById('editstatus_akun').value   = data.status_akun; 
                    document.getElementById('editlevel_akun').value    = data.level_akun;             
                    
                    $('#modalEditPengguna').modal('show');
                }
            
            },
            error: function (xhr) {                
                toastNotif('error', 'Terjadi kesalahan saat melakukan permintaan ke server', 'GAGAL', );                    
            }
        });
        // $('#modalEditPengguna').modal('show');
        
    }

    $(document).on('click', '.btn-edit-ruangan', function() {
        const baseUrlElement = document.getElementById('base-url');
        const baseUrl        = baseUrlElement.getAttribute('data-baseurl');
        const ruanganId = $(this).data('ruangan');
    
        $.ajax({
            url: baseUrl + '/ruangan-detail',
            data: { ruangan: ruanganId },
            type: 'GET',
            dataType: 'json',
            success: function(response) {
                if (response.statuslog == 'error') {
                    toastNotif('warning', response.message, 'GAGAL');

                } else {
                    const data = response;
    
                    // Mengisi form modal dengan data yang diperoleh dari server
                    $('#ruanganid').val(data.rgnid);
                    $('#edit-nama_ruangan').val(data.nama_ruangan);
                    $('#edit-warna_ruangan').val(data.warna_ruangan);
                    $('#edit-status_ruangan').val(data.status_ruangan);
                    $('#edit-jumlah_kursi').val(data.jumlah_kursi);
                    $('#edit-keterangan').val(data.keterangan);
                    $('#edit-fasilitas').val(data.fasilitas_lain);
    
                    // Menampilkan modal
                    $('#modalEditRuangan').modal('show');
                }
            },
            error: function(xhr) {
                toastNotif('error', 'Terjadi kesalahan saat mengambil data ruangan', 'ERROR');
            }
        });
    });

    $(document).on('click', '.btn-edit-booking', function() {
        const baseUrlElement = document.getElementById('base-url');
        const baseUrl        = baseUrlElement.getAttribute('data-baseurl');
        const bookingId = $(this).data('booking');
    
        $.ajax({
            url: baseUrl + '/booking-detail',
            data: { booking: bookingId },
            type: 'GET',
            dataType: 'json',
            success: function(response) {
                if (response.statuslog == 'error') {
                    toastNotif('warning', response.message, 'GAGAL');

                } else {
                    const data = response;
    
                    // Mengisi form modal dengan data yang diperoleh dari server
                    $('#bookingid').val(data.bookid);
                    $('#nama_kegiatan').val(data.tujuan_kegiatanbook);
                    $('#nama_ruangan').val(data.ruanganid).trigger('change');
                    $('#flatpickr-range').flatpickr({
                        defaultDate: [data.tanggal_mulai, data.tanggal_selesai],
                        mode: "range",
                        // minDate: "today",
                        locale: {
                            firstDayOfWeek: 1, // start week on Monday
                            rangeSeparator: ' sampai '
                        }
                    }).setDate([data.tanggal_mulai, data.tanggal_selesai]);
                    $('#flatpickr-time').flatpickr({
                        enableTime: true,
                        noCalendar: true,
                        dateFormat: "H:i",
                        time_24hr: true
                    }).setDate(data.waktu_mulai);
                    $('#time-input').val(data.durasi);
                    catatanEdit.then(editor => {
                        editor.setData(data.catatan);
                    });
    
                    // Menampilkan modal
                    $('#modalEditBooking').modal('show');
                }
            },
            error: function(xhr) {
                toastNotif('error', 'Terjadi kesalahan saat mengambil data ruangan', 'ERROR');
            }
        });
    });

    

    $("#edit-ruangan").submit(function(event) {
        event.preventDefault();
    
        const baseUrlElement = document.getElementById('base-url');
        const baseUrl        = baseUrlElement.getAttribute('data-baseurl');
    
        Swal.fire({
            title: '<h4>Edit Ruangan</h4>',
            icon: 'question',
            html: "Konfirmasi edit ruangan ini?",
            customClass: { 
                confirmButton: 'btn btn-primary',
                cancelButton: 'btn btn-danger',
            },
            showCancelButton: true,
            confirmButtonText: 'Ya, edit'
        }).then((result) => {
            if (result.isConfirmed) 
            {
                var formData = new FormData(this);
                $.ajax({
                    url: baseUrl + '/edit-ruangan',
                    type: "post",
                    data: formData,
                    dataType: 'json',
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function (response) {
                        
                        if (response.statuslog == 'error') {
                            toastNotif('warning', response.message, 'GAGAL');
                        } 
                        else {
                            toastNotif(response.statuslog, response.message, response.title);
                            $('#table-dataruangan').DataTable().ajax.reload();
                            $('#edit-ruangan')[0].reset();
                            $('#modalEditRuangan').modal('hide');
                        }
                        
                    },
                    error: function (xhr) {
                        toastNotif('error', 'Terjadi kesalahan saat melakukan permintaan ke server', 'ERROR');        
                    }
                });
        
            }
            else{
                toastNotif('info', 'Tindakan dibatalkan', 'INFO');
            }
        });
    });

    $(document).on('click', '.btn-detail-ruangan', function() {
        const baseUrlElement = document.getElementById('base-url');
        const baseUrl        = baseUrlElement.getAttribute('data-baseurl');
        const ruanganId = $(this).data('ruangan');
    
        $.ajax({
            url: baseUrl + '/ruangan-detail',
            data: { ruangan: ruanganId },
            type: 'GET',
            dataType: 'json',
            success: function(response) {
                if (response.statuslog == 'error') {
                    toastNotif('warning', response.message, 'GAGAL');

                } else {
                    const data = response;
    
                    // Mengisi form modal dengan data yang diperoleh dari server
                    document.getElementById('dtnama_ruangan').innerHTML = data.nama_ruangan;
                    document.getElementById('dtstatus_ruangan').innerHTML = data.status_ruangan;
                    document.getElementById('dtjumlah_kursi').innerHTML = data.jumlah_kursi;
                    document.getElementById('dtwarna_ruangan').innerHTML = '<div class="" style="width: 70px;height:30px;margin: auto;background-color:'+ data.warna_ruangan +'"></div>'
                    document.getElementById('dtfasilitas_lain').innerHTML = data.fasilitas_lain;
                    document.getElementById('dtketerangan').innerHTML = data.keterangan;
    
                    // Menampilkan modal
                    $('#modalDetailRuangan').modal('show');
                }
            },
            error: function(xhr) {
                toastNotif('error', 'Terjadi kesalahan saat mengambil data ruangan', 'ERROR');
            }
        });
    });
    
    $(document).on('click', '.btn-detail-booking', function() {
        const baseUrlElement = document.getElementById('base-url');
        const baseUrl        = baseUrlElement.getAttribute('data-baseurl');
        const bookingId      = $(this).data('booking');
    
        $.ajax({
            url: baseUrl + '/booking-detail',
            data: { booking: bookingId },
            type: 'GET',
            dataType: 'json',
            success: function(response) {
                if (response.statuslog == 'error') {
                    toastNotif('warning', response.message, 'GAGAL');

                } else {
                    const data = response;

                    document.getElementById('nama-kegiatan').textContent    = data.tujuan_kegiatanbook;
                    document.getElementById('jam').textContent              = data.jam;
                    document.getElementById('waktu').textContent            = data.waktu;
                    document.getElementById('lokasi').textContent           = data.nama_ruangan;
                    document.getElementById('deskripsi').innerHTML          = data.catatan;
                    document.getElementById('dibookingoleh').textContent    = data.nama_lengkap;
    
                    // Menampilkan modal
                    $('#modalDetailBooking').modal('show');
                }
            },
            error: function(xhr) {
                toastNotif('error', 'Terjadi kesalahan saat mengambil data ruangan', 'ERROR');
            }
        });
    });

    $(document).on('click', '#btn-modalbookingroom', function() {
        
        // $flatpickr.clear();
        $('#bookingRoomModal').modal('show');
    });

    $(document).on('click', '.btn-hapus-booking', function() {
        const baseUrlElement = document.getElementById('base-url');
        const baseUrl        = baseUrlElement.getAttribute('data-baseurl');

        const bookingId = $(this).data('booking');

        Swal.fire({
            title: '<h4>Hapus Booking Room</h4>',
            icon: 'warning',
            html: "Apakah anda ingin menghapus data booking ini ?",
            customClass: { 
                confirmButton: 'btn btn-primary',
                cancelButton: 'btn btn-danger',
            },
            showCancelButton: true,
            confirmButtonText: 'Ya, hapus'
        }).then((result) => {
            if (result.isConfirmed) 
            {
                $.ajax({
                    url: baseUrl + '/hapus-bookingroom',
                    type: 'GET',
                    dataType:"json",
                    data: { booking: bookingId, status: 'hapus'},
                    success: function (data) 
                    {
                        if (data.statuslog == 'error') {
                            toastNotif('warning', data.message, 'GAGAL');
        
                        } else {
                            $('#table-bookingroom').DataTable().ajax.reload();
                            toastNotif(data.statuslog, data.message, data.title);
                        }   
                    },
                    error: function(xhr) {
                        toastNotif('error', 'Terjadi kesalahan saat melakukan penghapusan data ruangan', 'ERROR');
                    }
                });
        
            }
            else{
                toastNotif('info', 'Tindakan dibatalkan', 'INFO');
            }
        });

               
    });

    $(document).on('change', '#nama_ruangan', function() {
        const selectRuangan     = document.getElementById('nama_ruangan');
        const informasiRuangan  = document.getElementById('informasi-ruangan');
        const jumlahKursi       = document.getElementById('jumlah-kursi');
        const fasilitasRuangan  = document.getElementById('fasilitas-ruangan');
        const inputanNamaRungan = document.getElementById('inputan-namaruangan');
        const selectedOption    = selectRuangan.options[selectRuangan.selectedIndex];

        if (this.value) 
        {
            const kursi     = selectedOption.getAttribute('data-kursi') || 0;
            const fasilitas = selectedOption.getAttribute('data-fasilitas');

            // Set informasi ruangan
            jumlahKursi.textContent = kursi + ' Kursi' ;
            fasilitasRuangan.textContent = fasilitas;

            // Tampilkan informasi ruangan
            informasiRuangan.classList.remove('d-none');
            inputanNamaRungan.classList.remove('mb-3');
        }
        else {
            informasiRuangan.classList.add('d-none');
            inputanNamaRungan.classList.add('mb-3');
        }
    });


    function updateCheckboxColors() {
        $('.input-filter').each(function() {
          const warna = $(this).data('warna');
          if ($(this).is(':checked')) {
            $(this).css('color', warna).removeClass('border-only');
          } else {
            $(this).css('border-color', warna).addClass('border-only');
          }
        });
      }
  
      // Initialize the colors for the checkboxes
      updateCheckboxColors();
  
      // Handle change events for individual checkboxes
      $('.input-filter').change(function() {
        const warna = $(this).data('warna');
        if ($(this).is(':checked')) {
          $(this).css('color', warna).removeClass('border-only');
        } else {
          $(this).css('border-color', warna).addClass('border-only');
        }
      });
  
      // Handle change event for the select-all checkbox
      $('#selectAllRuangan').change(function() {
        const isChecked = $(this).is(':checked');
        $('.input-filter').prop('checked', isChecked);
        updateCheckboxColors();
      });
  
      // Synchronize colors initially
      updateCheckboxColors();

    if (document.getElementById('catatan')) 
    {
        ClassicEditor.create(document.querySelector('#catatan'),{
            toolbar: [ 'undo', 'redo', '|', 'heading', '|', 'bold', 'italic', 'link', '|', 'numberedList', 'bulletedList', 'blockQuote',  ],
            
        }).catch(error => {
            console.error(error);
        });        
    }

    if (document.getElementById('catatan-edit')) 
    {
        var catatanEdit;
        catatanEdit = ClassicEditor.create(document.querySelector('#catatan-edit'),{
            toolbar: [ 'undo', 'redo', '|', 'heading', '|', 'bold', 'italic', 'link', '|', 'numberedList', 'bulletedList', 'blockQuote',  ],
            
        }).catch(error => {
            console.error(error);
        });
    }