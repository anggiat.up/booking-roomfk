<!DOCTYPE html>

<html
  lang="en"
  class="light-style layout-menu-fixed layout-compact"
  dir="ltr"
  data-theme="theme-default"
  data-assets-path=""
  data-template="horizontal-menu-template">
  <head>
    <meta charset="utf-8" />
    <meta
      name="viewport"
      content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0" />

    <title>{{ $nama_halaman }}  | Booking Room - FKUNRI</title>

    <meta name="description" content="" />

    <!-- Favicon -->
    <link rel="icon" type="image/x-icon" href="img/favicon/fk-icon.ico" />

    <!-- Fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
    <link
      href="https://fonts.googleapis.com/css2?family=Public+Sans:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500;1,600;1,700&ampdisplay=swap"
      rel="stylesheet" />

    <!-- Icons -->
    <link rel="stylesheet" href="vendor/fonts/fontawesome.css" />
    <link rel="stylesheet" href="vendor/fonts/tabler-icons.css" />
    <link rel="stylesheet" href="vendor/fonts/flag-icons.css" />

    <!-- Core CSS -->
    <link rel="stylesheet" href="vendor/css/rtl/core.css" class="template-customizer-core-css" />
    <link rel="stylesheet" href="vendor/css/rtl/theme-default.css" class="template-customizer-theme-css" />
    <link rel="stylesheet" href="css/demo.css" />

    <!-- Vendors CSS -->
    <link rel="stylesheet" href="vendor/libs/node-waves/node-waves.css" />
    <link rel="stylesheet" href="vendor/libs/perfect-scrollbar/perfect-scrollbar.css" />
    <link rel="stylesheet" href="vendor/libs/typeahead-js/typeahead.css" />
    <link rel="stylesheet" href="vendor/libs/apex-charts/apex-charts.css" />
    <link rel="stylesheet" href="vendor/libs/swiper/swiper.css" />
    <link rel="stylesheet" href="vendor/libs/datatables-bs5/datatables.bootstrap5.css" />
    <link rel="stylesheet" href="vendor/libs/datatables-responsive-bs5/responsive.bootstrap5.css" />
    <link rel="stylesheet" href="vendor/libs/datatables-checkboxes-jquery/datatables.checkboxes.css" />
    <link rel="stylesheet" href="vendor/libs/select2/select2.css" />
    <link rel="stylesheet" href="vendor/libs/tagify/tagify.css" />
    <link rel="stylesheet" href="vendor/libs/bootstrap-select/bootstrap-select.css" />
    <link rel="stylesheet" href="vendor/libs/typeahead-js/typeahead.css" />
    <link rel="stylesheet" href="vendor/libs/bootstrap-maxlength/bootstrap-maxlength.css" />

     
     <link rel="stylesheet" href="vendor/libs/fullcalendar/fullcalendar.css" />
     <link rel="stylesheet" href="vendor/libs/quill/editor.css" />
     <link rel="stylesheet" href="vendor/libs/@form-validation/umd/styles/index.min.css" />
     <link rel="stylesheet" href="{{ asset('/vendor/libs/sweetalert2/sweetalert2.css') }}" />
    <link rel="stylesheet" href="{{ asset('/vendor/libs/toastr/toastr.css') }}" />
    <link rel="stylesheet" href="{{ asset('/vendor/libs/animate-css/animate.css') }}" />
    <link rel="stylesheet" href="vendor/libs/flatpickr/flatpickr.css" />
    <link rel="stylesheet" href="vendor/libs/bootstrap-datepicker/bootstrap-datepicker.css" />
    <link rel="stylesheet" href="vendor/libs/bootstrap-daterangepicker/bootstrap-daterangepicker.css" />
    <link rel="stylesheet" href="vendor/libs/jquery-timepicker/jquery-timepicker.css" />
    <link rel="stylesheet" href="vendor/libs/pickr/pickr-themes.css" />
    <script src="https://cdn.ckeditor.com/ckeditor5/40.1.0/classic/ckeditor.js"></script>
    <link rel="stylesheet" href="css/custom-style.css">
    


 
     <!-- Page CSS -->
 
     
     <!-- Page CSS -->
     <link rel="stylesheet" href="vendor/css/pages/cards-advance.css" />
     <link rel="stylesheet" href="vendor/css/pages/app-calendar.css" />

    <!-- Helpers -->
    <script src="vendor/js/helpers.js"></script>
    <!--! Template customizer & Theme config files MUST be included after core stylesheets and helpers.js in the <head> section -->
    <!--? Template customizer: To hide customizer set displayCustomizer value false in config.js.  -->
    <script src="vendor/js/template-customizer.js"></script>
    <!--? Config:  Mandatory theme config file contain global vars & default theme options, Set your preferred theme option in this file.  -->
    <script src="js/config.js"></script>

    <style>
      .form-check-booking {
        appearance: none;
        width: 20px;
        height: 20px;
        border: 2px solid #ccc;
        border-radius: 4px;
        display: inline-block;
        vertical-align: middle;
        position: relative;
      }

      .form-check-booking:checked {
        background-color: currentColor;
        border-color: currentColor;
      }

      .form-check-booking.border-only {
        background-color: transparent;
      }

      .input-group-text {
            cursor: pointer;
      }

      
    </style>
  </head>

  <body>
    <!-- Layout wrapper -->
    <div class="layout-wrapper layout-navbar-full layout-horizontal layout-without-menu">
      <div class="layout-container">
        <div id="base-url" data-baseurl="{{ url('') }}" ></div>
        <!-- Navbar -->

        <nav class="layout-navbar navbar navbar-expand-xl align-items-center bg-navbar-theme" id="layout-navbar">
          <div class="container-xxl">
            <div class="navbar-brand app-brand demo d-none d-xl-flex py-0 me-4">
              <a href="{{ url('') }}" class="app-brand-link gap-2">
                <span class="app-brand-logo demo">
                    <img src="{{ asset('img/icons/brands/fk-logo.png') }}"  width="22" height="22" viewBox="0 0 32 22" fill="none" ></img>
                </span>
                <span class="app-brand-text demo menu-text fw-bold">Booking Room FKUNRI</span>
              </a>

              <a href="javascript:void(0);" class="layout-menu-toggle menu-link text-large ms-auto d-xl-none">
                <i class="ti ti-x ti-sm align-middle"></i>
              </a>
            </div>

            <div class="layout-menu-toggle navbar-nav align-items-xl-center me-3 me-xl-0 d-xl-none">
              <a class="nav-item nav-link px-0 me-xl-4" href="javascript:void(0)">
                <i class="ti ti-menu-2 ti-sm"></i>
              </a>
            </div>

            <div class="navbar-nav-right d-flex align-items-center" id="navbar-collapse">
              <ul class="navbar-nav flex-row align-items-center ms-auto">
                

                <!-- Style Switcher -->
                <li class="nav-item dropdown-style-switcher dropdown me-2 me-xl-0">
                  <a class="nav-link dropdown-toggle hide-arrow" href="javascript:void(0);" data-bs-toggle="dropdown">
                    <i class="ti ti-md"></i>
                  </a>
                  <ul class="dropdown-menu dropdown-menu-end dropdown-styles">
                    <li>
                      <a class="dropdown-item" href="javascript:void(0);" data-theme="light">
                        <span class="align-middle"><i class="ti ti-sun me-2"></i>Light</span>
                      </a>
                    </li>
                    <li>
                      <a class="dropdown-item" href="javascript:void(0);" data-theme="dark">
                        <span class="align-middle"><i class="ti ti-moon me-2"></i>Dark</span>
                      </a>
                    </li>
                    <li>
                      <a class="dropdown-item" href="javascript:void(0);" data-theme="system">
                        <span class="align-middle"><i class="ti ti-device-desktop me-2"></i>System</span>
                      </a>
                    </li>
                  </ul>
                </li>
                <!-- / Style Switcher-->



                <!-- User -->
                <li class="nav-item navbar-dropdown dropdown-user dropdown">
                  <a class="nav-link dropdown-toggle hide-arrow" href="javascript:void(0);" data-bs-toggle="dropdown">
                    <div class="avatar avatar-online">
                      <img src="img/avatars/1.png" alt class="h-auto rounded-circle" />
                    </div>
                  </a>
                  <ul class="dropdown-menu dropdown-menu-end">
                    @if (Auth::check())
                    <li>
                      <a class="dropdown-item" href="javascript:void(0);">
                        <div class="d-flex">
                          <div class="flex-shrink-0 me-3">
                            <div class="avatar avatar-online">
                              <img src="img/avatars/1.png" alt class="h-auto rounded-circle" />
                            </div>
                          </div>
                          <div class="flex-grow-1">
                            <span class="fw-medium d-block">{{ Auth::user()->nama_lengkap }}</span>
                            <small class="text-muted">{{ Auth::user()->level_akun }}</small>
                          </div>
                        </div>
                      </a>
                    </li>
                    <li>
                      <div class="dropdown-divider"></div>
                    </li>
                    <li>
                      <a class="dropdown-item" href="{{ url('profile') }}">
                        <i class="ti ti-user-check me-2 ti-sm"></i>
                        <span class="align-middle">My Profile</span>
                      </a>
                    </li>
                    <li>
                      <div class="dropdown-divider"></div>
                    </li>
                    <li>
                      <a class="dropdown-item" id="logout-btn" data-link="{{ url('logout') }}" href="javascript:void(0);">
                        <i class="ti ti-logout me-2 ti-sm"></i>
                        <span class="align-middle">Log Out</span>
                      </a>
                    </li>
                    @else
                    <li>
                      <a class="dropdown-item" href="javascript:void(0);"  data-bs-toggle="modal" data-bs-target="#loginModal">
                        <i class="ti ti-logout me-2 ti-sm"></i>
                        <span class="align-middle">Log In</span>
                      </a>
                    </li>
                    @endif
                  </ul>
                </li>
                <!--/ User -->
              </ul>
            </div>

            
          </div>
        </nav>

        <!-- / Navbar -->

        <!-- Layout container -->
        <div class="layout-page">
          <!-- Content wrapper -->
          <div class="content-wrapper">
            <!-- Menu -->
            <aside id="layout-menu" class="layout-menu-horizontal menu-horizontal menu bg-menu-theme flex-grow-0">
              <div class="container-xxl d-flex h-100">
                <ul class="menu-inner">

                  <!-- Dashboards -->
                  <li class="menu-item {{ menuActive($judulhalaman, 'dashboard'); }}">
                    <a href="{{ url('') }}" class="menu-link ">
                      <i class="menu-icon tf-icons ti ti-smart-home"></i>
                      <div data-i18n="Dashboards">Dashboards</div>
                    </a>                    
                  </li>

                 

                  <li class="menu-item {{ menuActive($judulhalaman, 'bookingroom'); }}">
                    <a href="{{ url('data-bookingroom') }}" class="menu-link ">
                      <i class="menu-icon tf-icons ti ti-calendar"></i>
                      <div data-i18n="Data Booking Room">Data Booking Room</div>
                    </a>                    
                  </li>

                  @if (Auth::check())
                    @if (Auth::user()->level_akun == 'adm')

                    <li class="menu-item {{ menuActive($judulhalaman, 'pengguna'); }}">
                      <a href="{{ url('data-pengguna') }}" class="menu-link ">
                        <i class="menu-icon tf-icons ti ti-calendar"></i>
                        <div data-i18n="Data User">Data User</div>
                      </a>                    
                    </li>

                    <li class="menu-item {{ menuActive($judulhalaman, 'ruangan'); }}">
                      <a href="{{ url('data-ruangan') }}" class="menu-link ">
                        <i class="menu-icon tf-icons ti ti-calendar"></i>
                        <div data-i18n="Ruangan">Ruangan</div>
                      </a>                    
                    </li>

                    @endif
                  @endif

           



                  
                </ul>
              </div>
            </aside>
            <!-- / Menu -->

            <!-- Content -->

            <div class="container-xxl flex-grow-1 container-p-y">
              <div class="row">
                
                @yield('container')

              </div>
            </div>
            <!--/ Content -->


            {{-- Modal Login --}}
            <div class="modal fade" id="loginModal" data-bs-backdrop="static" tabindex="-1" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                  </div>
                  <div class="modal-body">

                    <!-- Logo -->
                    <div class="app-brand justify-content-center mb-4 mt-2">
                      <a href="{{ url('') }}" class="app-brand-link gap-2">
                        <span class="app-brand-logo demo">
                          <svg width="22" height="22" viewBox="0 0 22 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                              <img src="{{ asset('img/icons/brands/fk-logo.png') }}"  width="22" height="22" viewBox="0 0 52 52" fill="none" ></img>
                          </svg>
                        </span>
                        <span class="app-brand-text demo text-body fw-bold ms-1">BOOKING ROOM</span>
                      </a>
                    </div>
                    <!-- /Logo -->
                    <form id="form-login" class="mb-3" action="#" method="POST">
                      <div class="mb-3">
                        <label for="email" class="form-label">Email</label>
                        @csrf
                        <input type="email" class="form-control" id="email" name="email" placeholder="Enter your email or username" autofocus />
                      </div>
                      <div class="mb-3 form-password-toggle">
                        <div class="input-group input-group-merge">
                          <input type="password" id="password" class="form-control" name="password" placeholder="&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;" aria-describedby="password" />
                          <span class="input-group-text cursor-pointer"><i class="ti ti-eye-off"></i></span>
                        </div>
                      </div>
                      <div class="mb-3">
                        <button class="btn btn-primary d-grid w-100" type="submit">Sign in</button>
                      </div>
                    </form>
                  </div>
                  
                </div>
              </div>
            </div>

            <!-- Footer -->
            <footer class="content-footer footer bg-footer-theme">
              <div class="container-xxl">
                <div
                  class="footer-container d-flex align-items-center justify-content-between py-2 flex-md-row flex-column">
                  <div>
                    ©2024 All rights reserved by ITFKUNRI
                  </div>
                  
                </div>
              </div>
            </footer>
            <!-- / Footer -->

            

            <div class="content-backdrop fade"></div>
          </div>
          <!--/ Content wrapper -->
        </div>

        <!--/ Layout container -->
      </div>
    </div>

    <!-- Overlay -->
    <div class="layout-overlay layout-menu-toggle"></div>

    <!-- Drag Target Area To SlideIn Menu On Small Screens -->
    <div class="drag-target"></div>

    <!--/ Layout wrapper -->

    <!-- Core JS -->
    <!-- build:js assets/vendor/js/core.js -->

    <script src="vendor/libs/jquery/jquery.js"></script>
    <script src="vendor/libs/popper/popper.js"></script>
    <script src="vendor/js/bootstrap.js"></script>
    <script src="vendor/libs/node-waves/node-waves.js"></script>
    <script src="vendor/libs/perfect-scrollbar/perfect-scrollbar.js"></script>
    <script src="vendor/libs/hammer/hammer.js"></script>
    <script src="vendor/libs/i18n/i18n.js"></script>
    <script src="vendor/js/menu.js"></script>

    <!-- endbuild -->

    <!-- Vendors JS -->
    <script src="vendor/libs/apex-charts/apexcharts.js"></script>
    <script src="vendor/libs/swiper/swiper.js"></script>
    <script src="vendor/libs/datatables-bs5/datatables-bootstrap5.js"></script>
    <script src="vendor/libs/fullcalendar/fullcalendar.js"></script>
    <script src="vendor/libs/@form-validation/umd/bundle/popular.min.js"></script>
    <script src="vendor/libs/@form-validation/umd/plugin-bootstrap5/index.min.js"></script>
    <script src="vendor/libs/@form-validation/umd/plugin-auto-focus/index.min.js"></script>
    
    <script src="{{ asset('/vendor/libs/sweetalert2/sweetalert2.js') }}"></script>
    <script src="{{ asset('/vendor/libs/toastr/toastr.js') }}"></script>

    <script src="vendor/libs/select2/select2.js"></script>
    <script src="vendor/libs/tagify/tagify.js"></script>
    <script src="vendor/libs/bootstrap-select/bootstrap-select.js"></script>
    <script src="vendor/libs/typeahead-js/typeahead.js"></script>
    <script src="vendor/libs/bloodhound/bloodhound.js"></script>

    <script src="vendor/libs/moment/moment.js"></script>
    <script src="vendor/libs/flatpickr/flatpickr.js"></script>
    <script src="vendor/libs/bootstrap-datepicker/bootstrap-datepicker.js"></script>
    <script src="vendor/libs/bootstrap-daterangepicker/bootstrap-daterangepicker.js"></script>
    <script src="vendor/libs/jquery-timepicker/jquery-timepicker.js"></script>
    <script src="vendor/libs/pickr/pickr.js"></script>
    <script src="vendor/libs/autosize/autosize.js"></script>


    <!-- Main JS -->
    <script src="js/main.js"></script>

    <!-- Page JS -->
    <script src="js/dashboards-analytics.js"></script>

    <script src="js/extended-ui-perfect-scrollbar.js"></script>
    <script src="js/forms-selects.js"></script>
    {{-- <script src="js/forms-tagify.js"></script> --}}
    <script src="js/forms-typeahead.js"></script>
    <script src="js/forms-pickers.js"></script>
    <script src="js/forms-extras.js"></script>
    

    <script src="{{ asset('/js/custome/form-script.js?v='.filemtime(public_path('/js/custome/form-script.js'))) }}"></script>
    <script src="{{ asset('/js/custome/function-script.js?v='.filemtime(public_path('/js/custome/function-script.js'))) }}"></script>
    <script src="{{ asset('/js/custome/datatable-script.js?v='.filemtime(public_path('/js/custome/datatable-script.js'))) }}"></script>
    <script src="{{ asset('/js/custome/calender-script.js?v='.filemtime(public_path('/js/custome/calender-script.js'))) }}"></script>

  </body>
</html>
