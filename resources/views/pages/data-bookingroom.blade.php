@extends('layout.mainbody')

@section('container')


    <div class="card">
        <h5 class="card-header">Data Booking Room</h5>
        <div class="card-datatable text-nowrap">


            <table id="table-bookingroom" class="table table-bordered datatables-ajax table">
                <thead>
                  <tr>
                      <th class="text-center">Nama Ruangan</th>
                      <th class="text-center">Created At</th>
                      <th class="text-center">Rentang Tanggal</th>
                      <th class="text-center">Rentang Waktu</th>
                      <th class="text-center">Nama Kegiatan</th>
                      <th class="text-center">Dibooking</th>
                      <th class="text-center">Aksi</th>
                  </tr>
                </thead>
            </table>
        </div>
    </div>

   

    <div class="modal fade" id="modalDetailBooking" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel1">Detail Booking</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
            
                <div class="modal-body">
                    

                    <table class="table">
                        <tbody>
                            <tr>
                                <th scope="row" width="30%"><b>NAMA KEGIATAN</b></th>
                                <td id="nama-kegiatan"></td>
                            </tr>
                            <tr>
                                <th scope="row"><b>JAM BOOKING</b></th>
                                <td id="jam"></td>
                            </tr>
                            <tr>
                                <th scope="row"><b>WAKTU BOOKING</b></th>
                                <td id="waktu"></td>
                            </tr>
                            <tr>
                                <th scope="row"><b>RUANGAN</b></th>
                                <td id="lokasi"></td>
                            </tr>
                            <tr>
                                <th scope="row"><b>DESKRIPSI</b></th>
                                <td id="deskripsi"></td>
                            </tr>
                            <tr>
                                <th scope="row"><b>DIBOOKING OLEH</b></th>
                                <td id="dibookingoleh"></td>
                            </tr>
                        </tbody>
                    </table>

                    

                </div>
            
                <div class="modal-footer">
                    <button type="reset" class="btn btn-label-secondary" data-bs-dismiss="modal">
                        Tutup
                    </button>
                </div>
          </div>
        </div>
    </div>

    <div class="modal fade" id="modalEditBooking" tabindex="-1" aria-labelledby="modalEditBookingLabel" data-bs-backdrop="static" aria-hidden="true">
        <div class="modal-dialog modal-xl ">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="eventDetailsModalLabel">Edit Booking Room</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form id="bookingRoomFormEdit" action="#" method="post">
                  <div class="modal-body" id="bookingFormBody">
                      @csrf
                        <div class="mb-3 row">
                          <label for="nama_kegiatan" class="col-md-3 col-form-label">Nama Kegiatan</label>
                          <div class="col-md-9">
                            <input class="form-control" type="text"  id="nama_kegiatan" name="nama_kegiatan" required />
                            <input type="hidden" name="bookingid" id="bookingid">
                            <input type="hidden" name="pengguna" value="{{ Auth::check() ? Auth::user()->pgnid : null }}">
                          </div>
                        </div>
  
                        <div class="mb-3 row" id="inputan-namaruangan">
                          <label for="nama_ruangan" class="col-md-3 col-form-label">Ruangan</label>
                          <div class="col-md-9">
                            <select class="select2 form-select form-select-lg" id="nama_ruangan" name="nama_ruangan" data-allow-clear="true" required>
                              <option value="" selected disabled hidden>-- Pilih Ruangan --</option>
                              @foreach ($dataruangan as $ruangan)
                                <option value="{{ $ruangan->rgnid }}" data-kursi="{{ $ruangan->jumlah_kursi }}" data-fasilitas="{{ $ruangan->fasilitas_lain }}">{{ $ruangan->nama_ruangan }}</option>
                              @endforeach
                            </select>
                          </div>
                        </div>

                        <div class="mb-3 row d-none" id="informasi-ruangan">
                          <div class="col-md-9 offset-md-3">
                            <div class="demo-inline-spacing mb-3">
                                <ul class="list-group">
                                    <li class="list-group-item d-flex align-items-center" id="info-kursi">
                                        <i class="ti ti-armchair ti-sm me-3"></i>
                                        <span id="jumlah-kursi">0 Kursi</span>
                                    </li>
                                    <li class="list-group-item d-flex align-items-center" id="info-fasilitas">
                                        <i class="ti ti-home-cog ti-sm me-3"></i>
                                        <span id="fasilitas-ruangan">Fasilitas yang tersedia</span>
                                    </li>
                                </ul>
                            </div>
                          </div>
                        </div>
  
  
                        <div class="mb-3 row">
                          <label for="nama_kegiatan" class="col-md-3 col-form-label">Tanggal Mulai & Selesai</label>
  
                          <div class="col-md-9">
                            <input type="text" class="form-control text-center" placeholder="{{ date('d-m-Y') }}" name="rentang_tanggal" id="flatpickr-range" required />
                            <small class="text-danger">*Klik tanggal 2 kali untuk dihari yang sama</small>
                          </div>
                          
                        </div>
  
                        <div class="mb-3 row">
                          <label for="nama_kegiatan" class="col-md-3 col-form-label">Waktu Mulai & Durasi</label>
                          <div class="col-md-5">
                            <input type="text" class="form-control text-center" placeholder="HH:MM" name="waktu_mulai" id="flatpickr-time" required />
                          </div>
                          <div class="col-md-4">
                            <div class="input-group">
                              <span class="input-group-text bg-primary text-white" id="minus-btn"><i class="fas fa-minus"></i></span>
                                <input type="number" class="form-control text-center" name="durasi" id="time-input" value="0" min="15" max="480" placeholder="/ menit" required />
                              <span class="input-group-text" >Menit</i></span>
                              <span class="input-group-text bg-primary text-white" id="plus-btn"><i class="fas fa-plus"></i></span>
                            </div>
                          </div>
                        </div>
  
                        <div class="mb-3 row">
                          <label for="catatan" class="col-md-3 col-form-label">Catatan</label>
                          <div class="col-md-9">
                            <textarea id="catatan-edit" name="catatan"  rows="3" class="form-control" ></textarea>
                          </div>
                        </div>
                        
  
  
                    </div>
                    <div class="modal-footer">
                      <button type="reset" class="btn btn-secondary" >Reset</button>
                      <button type="submit" class="btn btn-primary" >Simpan</button>
                    </div>
                  
                </form>
            </div>
        </div>
    </div>

   

@endsection