@extends('layout.mainbody')

@section('container')


    <div class="card">
        <h5 class="card-header">Data Ruangan</h5>
        <div class="card-datatable text-nowrap">

            <button type="button" class="btn btn-label-primary mb-4 " data-bs-toggle="modal" data-bs-target="#modalTambahRuangan">
                Tambah Ruangan
            </button>

            <table id="table-dataruangan" class="datatables-ajax table">
                <thead>
                <tr>
                    <th class="text-left">Nama Ruangan</th>
                    <th class="text-center">Status Ruangan</th>
                    <th class="text-center">Jumlah Kursi</th>
                    <th class="text-center">Warna Ruangan</th>
                    <th class="text-left">Keterangan</th>
                    <th class="text-center">Aksi</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>

   
    <div class="modal fade" id="modalTambahRuangan" tabindex="-1" aria-labelledby="modalTambahRuanganLabel" data-bs-backdrop="static" aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalTambahRuanganLabel">Form Tambah Ruangan</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form class="form" action="post" id="tambah-ruangan">
                    <div class="modal-body">
                        @csrf

                        <div class="row">
                            <div class="col mb-3">
                                <label class="form-label" for="nama_ruangan">Nama Ruangan</label>
                                <input class="form-control" placeholder="" type="text" name="nama_ruangan" id="nama_ruangan" required />                 
                            </div>
                        </div>

                        <div class="row">
                            <div class="col mb-3">
                                <label class="form-label" for="status_ruangan">Status Ruangan</label>
                                <select name="status_ruangan" id="status_ruangan" class="form-control">
                                    <option value="" selected disabled hidden>-- Status Ruangan --</option>
                                    <option value="tersedia">Tersedia</option>
                                    <option value="tidak-tersedia">Tidak Tersedia</option>
                                    <option value="maintenance">Maintenance</option>
                                </select>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col mb-3">
                                <label class="form-label" for="jumlah_kursi">Jumlah Kursi</label>
                                <input class="form-control" placeholder="" type="number" name="jumlah_kursi" id="jumlah_kursi" required />                 
                            </div>
                        </div>

                        <div class="row">
                            <div class="col mb-3">
                                <label class="form-label" for="fasilitas">Fasilitas</label>
                                <input class="form-control" placeholder="" type="text" name="fasilitas" id="fasilitas" required />                 
                            </div>
                        </div>

                        <div class="row">
                            <div class="col mb-3">
                                <label class="form-label" for="warna_ruangan">Warna Ruangan</label>
                                <input class="form-control" type="color" value="#666EE8" name="warna_ruangan" id="warna_ruangan" required />
                            </div>
                        </div>


                        <div class="row">
                            <div class="col mb-3">
                                <label class="form-label" for="keterangan">Keterangan</label>
                                <textarea id="autosize-demo" name="keterangan" id="keterangan" rows="3" class="form-control"></textarea>
                            </div>
                        </div>

                     

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Tambah Data Ruangan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalEditRuangan" tabindex="-1" aria-labelledby="modalEditRuanganLabel" data-bs-backdrop="static" aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalEditRuanganLabel">Form Edit Ruangan</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form class="form" action="post" id="edit-ruangan">
                    <div class="modal-body">
                        @csrf

                        <div class="row">
                            <div class="col mb-3">
                                <label class="form-label" for="nama_ruangan">Nama Ruangan</label>
                                <input class="form-control" placeholder="" type="text" name="nama_ruangan" id="edit-nama_ruangan" required />
                                <input type="hidden" value="" name="ruanganid" id="ruanganid">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col mb-3">
                                <label class="form-label" for="status_ruangan">Status Ruangan</label>
                                <select name="status_ruangan" id="edit-status_ruangan" class="form-control">
                                    <option value="" selected disabled hidden>-- Status Ruangan --</option>
                                    <option value="tersedia">Tersedia</option>
                                    <option value="tidak-tersedia">Tidak Tersedia</option>
                                    <option value="maintenance">Maintenance</option>
                                </select>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col mb-3">
                                <label class="form-label" for="jumlah_kursi">Jumlah Kursi</label>
                                <input class="form-control" placeholder="" type="number" name="jumlah_kursi" id="edit-jumlah_kursi" required />                 
                            </div>
                        </div>

                        <div class="row">
                            <div class="col mb-3">
                                <label class="form-label" for="fasilitas">Fasilitas</label>
                                <input class="form-control" placeholder="" type="text" name="fasilitas" id="edit-fasilitas" required />                 
                            </div>
                        </div>

                        <div class="row">
                            <div class="col mb-3">
                                <label class="form-label" for="warna_ruangan">Warna Ruangan</label>
                                <input class="form-control" type="color" value="#666EE8" name="warna_ruangan" id="edit-warna_ruangan" required />
                            </div>
                        </div>


                        <div class="row">
                            <div class="col mb-3">
                                <label class="form-label" for="keterangan">Keterangan</label>
                                <textarea name="keterangan" id="edit-keterangan" rows="3" class="form-control"></textarea>
                            </div>
                        </div>

                     

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
                        <button type="submit" class="btn btn-primary">Edit Data Ruangan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalDetailRuangan" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel1">Detail Ruangan</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
            
                <div class="modal-body">
                    

                    <table class="table">
                        <tbody>
                            <tr>
                                <th scope="row" width="30%"><b>Nama Ruangan</b></th>
                                <td id="dtnama_ruangan"></td>
                            </tr>
                            <tr>
                                <th scope="row" width="30%"><b>Status Ruangan</b></th>
                                <td id="dtstatus_ruangan"></td>
                            </tr>
                            <tr>
                                <th scope="row" width="30%"><b>Jumlah Kursi</b></th>
                                <td id="dtjumlah_kursi"></td>
                            </tr>
                            <tr>
                                <th scope="row" width="30%"><b>Warna Ruangan</b></th>
                                <td id="dtwarna_ruangan"></td>
                            </tr>
                            <tr>
                                <th scope="row" width="30%"><b>Fasilitas Lain</b></th>
                                <td id="dtfasilitas_lain"></td>
                            </tr>
                            <tr>
                                <th scope="row" width="30%"><b>Keterangan</b></th>
                                <td id="dtketerangan"></td>
                            </tr>
                        </tbody>
                    </table>

                    

                </div>
            
                <div class="modal-footer">
                    <button type="reset" class="btn btn-label-secondary" data-bs-dismiss="modal">
                        Tutup
                    </button>
                </div>
          </div>
        </div>
    </div>

   

@endsection