@extends('layout.mainbody')

@section('container')


    <div class="card">
        <h5 class="card-header">Data Akun Pengguna</h5>
        <div class="card-datatable text-nowrap">
        <table id="table-datapengguna" class="datatables-ajax table">
            <thead>
            <tr>
                <th class="text-left">Nama Lengkap</th>
                <th class="text-center">Level</th>
                <th class="text-center">Status Akun</th>
                <th class="text-left">Email</th>
                <th class="text-center">Aksi</th>
            </tr>
            </thead>
        </table>
        </div>
    </div>

   

    <!-- Modal -->
    <div class="modal fade" id="modalDetailpengguna" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel1">Detail Pengguna</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
            
                <div class="modal-body">
                    

                    <table class="table">
                        <tbody>
                            <tr>
                                <th scope="row" width="30%"><b>Nama Lengkap</b></th>
                                <td id="nama_lengkap"></td>
                            </tr>
                            <tr>
                                <th scope="row" width="30%"><b>Email</b></th>
                                <td id="email_pengguna"></td>
                            </tr>
                            <tr>
                                <th scope="row" width="30%"><b>Nomor Hp</b></th>
                                <td id="no_hp"></td>
                            </tr>
                            <tr>
                                <th scope="row" width="30%"><b>Level Akun</b></th>
                                <td id="level_akun"></td>
                            </tr>
                            <tr>
                                <th scope="row" width="30%"><b>Status Akun</b></th>
                                <td id="status_akun"></td>
                            </tr>
                        </tbody>
                    </table>

                    

                </div>
            
                <div class="modal-footer">
                    <button type="reset" class="btn btn-label-secondary" data-bs-dismiss="modal">
                        Close
                    </button>
                </div>
          </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="modalEditPengguna" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog " role="document">
          <div class="modal-content">
            <form class="form" action="post" id="update-datapengguna">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel1">Form Edit Pengguna</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
            
                <div class="modal-body">
                    @csrf
                    <div class="row">
                        <div class="col mb-3">
                            <label class="form-label">Nama Lengkap</label>
                            <input class="form-control" placeholder="" type="text" name="nama_lengkap" id="editnama_lengkap" required />
                            <input type="hidden" name="pengguna_id" id="pengguna_id">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col mb-3">
                            <label class="form-label">Email</label>
                            <input class="form-control" placeholder="" type="email" name="email" id="editemail" required />
                        </div>
                    </div>

                    <div class="row">
                        <div class="col mb-3">
                            <label class="form-label">No. Handphone</label>
                            <input class="form-control" placeholder="" type="text" name="no_hp" id="editno_hp" required />
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col mb-3">
                            <label class="form-label">Level Akun</label>
                            <select class="form-select" name="level_akun" id="editlevel_akun" >
                                <option></option>
                                <option value="pgn">Pengguna</option>
                                <option value="opkg">Admin Kegiatan</option>
                                <option value="adm">Administrator</option>
                            </select>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col mb-3">
                            <label class="form-label">Status Akun</label>
                            <select class="form-select" name="status_akun" id="editstatus_akun" >
                                <option></option>
                                <option value="aktif">Aktif</option>
                                <option value="tidak-aktif">Tidak Aktif</option>
                            </select>
                        </div>
                    </div>

                </div>
            
                <div class="modal-footer">
                    <button type="reset" class="btn btn-label-secondary" data-bs-dismiss="modal">
                        Close
                    </button>
                    <button type="submit" class="btn btn-primary">Update Pengguna</button>
                </div>
            </form>
          </div>
        </div>
    </div>


@endsection