@extends('layout.mainbody')

@section('container')

<!-- Website Analytics -->

    {{-- <div class="col-lg-3 col-sm-6 mb-4">
      <div class="card h-100">
        <div class="card-body d-flex justify-content-between align-items-center">
          <div class="card-title mb-0">
            <h5 class="mb-0 me-2">86%</h5>
            <small>Total Booking Bulan {{ date('F') }}</small>
          </div>
          <div class="card-icon">
            <span class="badge bg-label-primary rounded-pill p-2">
              <i class="ti ti-cpu ti-sm"></i>
            </span>
          </div>
        </div>
      </div>
    </div>
    <div class="col-lg-3 col-sm-6 mb-4">
      <div class="card h-100">
        <div class="card-body d-flex justify-content-between align-items-center">
          <div class="card-title mb-0">
            <h5 class="mb-0 me-2">128</h5>
            <small>Jumlah Booking Hari Ini</small>
          </div>
          <div class="card-icon">
            <span class="badge bg-label-warning rounded-pill p-2">
              <i class="ti ti-alert-octagon ti-sm"></i>
            </span>
          </div>
        </div>
      </div>
    </div>
    <div class="col-lg-3 col-sm-6 mb-4">
      <div class="card h-100">
        <div class="card-body d-flex justify-content-between align-items-center">
          <div class="card-title mb-0">
            <h5 class="mb-0 me-2">1.24gb</h5>
            <small>Ruangan yang Tersedia</small>
          </div>
          <div class="card-icon">
            <span class="badge bg-label-success rounded-pill p-2">
              <i class="ti ti-server ti-sm"></i>
            </span>
          </div>
        </div>
      </div>
    </div>
    <div class="col-lg-3 col-sm-6 mb-4">
      <div class="card h-100">
        <div class="card-body d-flex justify-content-between align-items-center">
          <div class="card-title mb-0">
            <h5 class="mb-0 me-2">0.2%</h5>
            <small>Ruangan yang Digunakan</small>
          </div>
          <div class="card-icon">
            <span class="badge bg-label-danger rounded-pill p-2">
              <i class="ti ti-chart-pie-2 ti-sm"></i>
            </span>
          </div>
        </div>
      </div>
    </div> --}}
    
    

    <div class="col-xl-12 mb-4">
        <div class="card app-calendar-wrapper">
            <div class="row g-0">
            <!-- Calendar Sidebar -->
              <div class="col col-lg-5 app-calendar-sidebar" id="app-calendar-sidebar">
                <div class="border-bottom p-4 my-sm-0 mb-3">
                  <div class="d-grid">
                      {{-- <button class="btn btn-primary btn-toggle-sidebar" data-bs-toggle="offcanvas" data-bs-target="#addEventSidebar" aria-controls="addEventSidebar">
                        <i class="ti ti-plus me-1"></i>
                        <span class="align-middle">Add Event</span>
                      </button> --}}

                      <button class="btn btn-primary btn-toggle-sidebar" id="btn-modalbookingroom" data-bs-toggle="modal" data-bs-target="#bookingRoomModal">
                      {{-- <button class="btn btn-primary btn-toggle-sidebar" id="btn-modalbookingroom" > --}}
                        <i class="ti ti-plus me-1"></i>
                        <span class="align-middle">Booking Room</span>
                      </button>
                  </div>
                </div>
                <div class="p-3">
                <!-- inline calendar (flatpicker) -->
                <div class="" id="inline-calendar-book"></div>

                <hr class="container-m-nx mb-4 mt-3" />

                <!-- Filter -->
                <div class="mb-3 ms-3 text-center">
                    <h5 class=" text-uppercase align-middle">DAFTAR RUANGAN</h5>
                </div>

                
                <div id="vertical-example" class="app-calendar-events-filter ms-3" style="height: 400px">
                  
                  <div class="form-check mb-2">
                      <input class="form-check-input form-check-booking input-filter select-all-ruangan" type="checkbox" id="selectAllRuangan" data-value="all" data-warna="#7367F0" checked />
                      <label class="form-check-label font-weight-bold" for="selectAllRuangan"><b>Semua Ruangan</b></label>
                  </div>
                  @php
                      $checkbox = 1;
                  @endphp
                  @foreach ($dataruangan as $ruangan)

                      <div class="form-check mb-2">
                          <input class="form-check-input input-filter form-check-booking" type="checkbox" name="data-ruangan" id="ckbox{{ $checkbox; }}" value="{{ $ruangan->nama_ruangan }}" data-warna="{{ $ruangan->warna_ruangan }}" checked />
                          <label class="form-check-label" for="ckbox{{ $checkbox; }}">{{ limitText($ruangan->nama_ruangan, 20) }}</label>
                      </div>
                      @php

                          $checkbox++;

                      @endphp
                  @endforeach
                

                </div>

              </div>
            </div>
            <!-- /Calendar Sidebar -->

            <!-- Calendar & Modal -->
            <div class="col app-calendar-content">
                <div class="card shadow-none border-0">
                  <div class="card-body pb-0">
                      <!-- FullCalendar -->
                      <div id="calendar-booking"></div>
                  </div>
                </div>
                
            </div>
            <!-- /Calendar & Modal -->
            </div>
        </div>
    </div>

    <!-- Modal Tmbah Booking Room -->
    <div class="modal fade" id="bookingRoomModal" tabindex="-1" aria-labelledby="bookingRoomModalLabel" data-bs-backdrop="static" aria-hidden="true">
      <div class="modal-dialog modal-xl ">
          <div class="modal-content">
              <div class="modal-header">
                  <h5 class="modal-title" id="eventDetailsModalLabel">Booking Room</h5>
                  <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
              </div>
              <form id="bookingRoomForm" action="#" method="post">
                <div class="modal-body" id="bookingFormBody">
                    @if (Auth::check())
                      @csrf
                      

                      <div class="mb-3 row">
                        <label for="nama_kegiatan" class="col-md-3 col-form-label">Nama Kegiatan <strong class="text-danger">*</strong> </label>
                        <div class="col-md-9">
                          <input class="form-control" type="text"  id="nama_kegiatan" name="nama_kegiatan" required />
                          <input type="hidden" name="pengguna" value="{{ Auth::check() ? Auth::user()->pgnid : null }}">
                        </div>
                      </div>

                      <div class="mb-3 row" id="inputan-namaruangan">
                        <label for="nama_ruangan" class="col-md-3 col-form-label">Ruangan <strong class="text-danger">*</strong></label>
                        <div class="col-md-9">
                          <select class="select2 form-select form-select-lg" id="nama_ruangan" name="nama_ruangan" data-allow-clear="true" required>
                            <option value="" selected disabled hidden>-- Pilih Ruangan --</option>
                            @foreach ($dataruangan as $ruangan)
                              <option value="{{ $ruangan->rgnid }}" data-kursi="{{ $ruangan->jumlah_kursi }}" data-fasilitas="{{ $ruangan->fasilitas_lain }}">{{ $ruangan->nama_ruangan }}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>

                      <div class="mb-3 row d-none" id="informasi-ruangan">
                        <div class="col-md-9 offset-md-3">
                          <div class="demo-inline-spacing mb-3">
                              <ul class="list-group">
                                  <li class="list-group-item d-flex align-items-center" id="info-kursi">
                                      <i class="ti ti-armchair ti-sm me-3"></i>
                                      <span id="jumlah-kursi">0 Kursi</span>
                                  </li>
                                  <li class="list-group-item d-flex align-items-center" id="info-fasilitas">
                                      <i class="ti ti-home-cog ti-sm me-3"></i>
                                      <span id="fasilitas-ruangan">Fasilitas yang tersedia</span>
                                  </li>
                              </ul>
                          </div>
                        </div>
                      </div>


                      <div class="mb-3 row">
                        <label for="nama_kegiatan" class="col-md-3 col-form-label">Tanggal Mulai & Selesai <strong class="text-danger">*</strong></label>
                        <div class="col-md-9">
                          <input type="text" class="form-control text-center" placeholder="{{ date('d-m-Y') }}" name="rentang_tanggal" id="flatpickr-range" required />
                          <small class="text-danger">*Klik tanggal 2 kali untuk dihari yang sama</small>
                        </div>
                        
                      </div>

                      <div class="mb-3 row">
                        <label for="nama_kegiatan" class="col-md-3 col-form-label">Waktu Mulai & Durasi <strong class="text-danger">*</strong></label>
                        <div class="col-md-5">
                          <input type="text" class="form-control text-center" placeholder="HH:MM" name="waktu_mulai" id="flatpickr-time" required />
                        </div>
                        <div class="col-md-4">
                          <div class="input-group">
                            <span class="input-group-text bg-primary text-white" id="minus-btn"><i class="fas fa-minus"></i></span>
                              <input type="number" class="form-control text-center" name="durasi" id="time-input" value="0" min="15" max="480" placeholder="/ menit" required />
                            <span class="input-group-text" >Menit</i></span>
                            <span class="input-group-text bg-primary text-white" id="plus-btn"><i class="fas fa-plus"></i></span>
                          </div>
                        </div>
                      </div>

                      
                      <div class="mb-3 row">
                        <label for="catatan" class="col-md-3 col-form-label">Catatan <strong class="text-danger">*</strong></label>
                        <div class="col-md-9">
                          <textarea name="catatan" id="catatan" rows="3" class="form-control" ></textarea>                          
                        </div>
                      </div>
                      <div class="mb-3 row">
                        <label for="catatan" class="col-md-3 col-form-label"></label>
                        <label for="catatan" class="col-md-9 col-form-label"><strong class="text-danger">* Wajib diiisi</strong></label>                        
                      </div>
                      @else
                      <div class="alert alert-warning d-flex align-items-center text-dark" role="alert">
                        <span class="alert-icon text-warning me-2">
                          <i class="ti ti-bell ti-xs"></i>
                        </span>
                        Silahkan melakukan login, sebelum melakukan bookingroom
                      </div>

                      <a class="btn btn-primary text-center" href="javascript:void(0);"  data-bs-toggle="modal" data-bs-target="#loginModal">
                        <i class="ti ti-logout me-2 ti-sm"></i>
                        <span class="align-middle">Log In</span>
                      </a>
                    @endif


                  </div>
                  @if (Auth::check())
                  <div class="modal-footer">
                    <button type="reset" class="btn btn-secondary" >Reset</button>
                    <button type="submit" class="btn btn-primary" >Simpan</button>
                  </div>
                  @endif
                
              </form>
          </div>
      </div>
    </div>


    <div class="modal fade" id="eventDetailsBookModal" tabindex="-1" aria-labelledby="eventDetailsBookModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg modal-dialog-centered">
          <div class="modal-content">
              <div class="modal-header">
                  <h5 class="modal-title" id="eventDetailsBookModalLabel">Detail Booking</h5>
                  <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
              </div>
              <div class="modal-body" id="eventDetailsBodyRegistrasi">
                <div class="table-responsive">

                <table class="table table-sm">
                  <tbody>
                      <tr>
                          <th scope="row" width="30%"><b>NAMA KEGIATAN</b></th>
                          <td id="nama-kegiatan"></td>
                      </tr>
                      <tr>
                          <th scope="row"><b>WAKTU BOOKING</b></th>
                          <td id="waktu"></td>
                      </tr>
                      <tr>
                        <th scope="row"><b>TANGGAL</b></th>
                        <td id="tanggal"></td>
                    </tr>
                      <tr>
                          <th scope="row"><b>RUANGAN</b></th>
                          <td id="lokasi"></td>
                      </tr>
                      <tr>
                          <th scope="row"><b>CATATAN</b></th>
                          <td id="deskripsi"></td>
                      </tr>
                      <tr>
                          <th scope="row"><b>DIBOOKING OLEH</b></th>
                          <td id="dibookingoleh"></td>
                      </tr>
                  </tbody>
                </table>
              </div>

              </div>                      
          </div>
      </div>
    </div>

   

    <div class="col-xl-12 mb-4">
      <div class="d-grid gap-2 col-lg-6 mx-auto d-lg-none">
        <button class="btn btn-primary btn-lg" type="button" data-bs-toggle="modal" data-bs-target="#bookingRoomModal">
          <i class="ti ti-plus me-1"></i> Booking Room
        </button>
      </div>
    </div>
    

    <div class="col-xl-12 mb-4">
        <div class="card">
          <h5 class="card-header">Riwayat Pengajuan Booking Room</h5>
          <div class="card-body pb-0">

            <ul class="timeline mb-0">

              @foreach ($databooking as $bkd)
              <li class="timeline-item timeline-item-transparent mt-5 bg-label-info p-8 rounded">
                <span class="timeline-point timeline-point-primary"></span>
                <div class="timeline-event">
                  <div class="timeline-header mb-3 mt-3">
                    <h6 class="mb-0"><b>{{ $bkd->pengguna->nama_lengkap }} : </b>Melakukan Booking Room</h6>
                    <small class="text-muted">{{ date('H:i d-F-Y', strtotime($bkd->created_at)) }}</small>
                  </div>
                  <hr>
                  <div class="d-flex align-items-center mb-2">
                    <div class="table-responsive">
                      <table class="table table-sm table-borderless" style="min-width: 600px;" width="100%">
                        <tr>
                          <th class="align-middle" width="20%"><b>LOKASI</b></th>
                          <td class="align-middle" width="5px">:</td>
                          <td class="align-middle" style="">{{ $bkd->ruangan->nama_ruangan }}</td>
                        </tr>
                        <tr>
                          <th class="align-middle" ><b>WAKTU</b></th>
                          <td class="align-middle" >:</td>
                          <td class="align-middle" width="">{{ date('d-F-Y', strtotime($bkd->tanggal_mulai)) }}</td>
                        </tr>
                        <tr>
                          <th class="align-middle" ><b>NAMA KEGIATAN</b></th>
                          <td class="align-middle" >:</td>
                          <td class="align-middle" width="">{{ $bkd->tujuan_kegiatanbook }}</td>
                        </tr>
                      </table>
                    </div>
                  </div>
                </div>
              </li>
              @endforeach
              
            </ul>


          </div>
        </div>
    </div>

    <style>
      .fc-daygrid-day-frame {
          /* height: 120px; */
      }

    </style>

@endsection