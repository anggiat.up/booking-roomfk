<?php
    use Illuminate\Support\Facades\Auth;
    use Illuminate\Support\Str;
use Ramsey\Uuid\Uuid;

    if (!function_exists('formatLevel')) {
        function formatLevel($level)
        {
            if ($level == 'pgn') {
                return 'Pengguna';
            }
            elseif ($level == 'opkg') {
                return 'Admin Kegiatan';
            }
            elseif ($level == 'adm') {
                return 'Administrator';
            }
            else{
                return 'Level Tidak Diketahui';
            }
        }
    }

    if (!function_exists('getRentang')) {
        function getRentang($range, $type = 'awal') {
            // Pecah rentang berdasarkan kata "sampai"
            $startDate = NULL;
            $endDate   = NULL;
            $dates     = explode(' sampai ', $range);
        
            if (count($dates) == 2) {
                $startDate = trim($dates[0]);
                $endDate   = trim($dates[1]);
            }
            elseif (count($dates) == 1){
                $startDate = trim($dates[0]);
                $endDate   = trim($dates[0]);
            }
            else{
                throw new Exception("Format rentang tidak valid.");
            }
        
            // Cek tipe yang diminta
            if ($type === 'awal') {
                return date('Y-m-d', strtotime($startDate));
            } elseif ($type === 'akhir') {
                return date('Y-m-d', strtotime($endDate));
            } else {
                throw new Exception("Tipe rentang tidak valid. Harus 'awal' atau 'akhir'.");
            }
        }
    }

    if (!function_exists('checkBrightness')) {
        function checkBrightness($colorCode)
        {
            // Memisahkan nilai RGB dari kode warna
            $r = hexdec(substr($colorCode, 1, 2));
            $g = hexdec(substr($colorCode, 3, 2));
            $b = hexdec(substr($colorCode, 5, 2));

            // Menghitung kecerahan berdasarkan formula perhitungan sederhana
            $brightness = (($r * 299) + ($g * 587) + ($b * 114)) / 1000;

            // Menentukan output berdasarkan kecerahan
            if ($brightness >= 128) {
                return '#262625'; // Kode warna untuk teks jika kecerahan terang
            } else {
                return '#fff'; // Kode warna untuk teks jika kecerahan rendah atau gelap
            }
        }
    }

    if (!function_exists('generateUuid')) {
        function generateUuid($name) {
            // Namespace UUID untuk aplikasi Anda (gunakan UUID v4 acak untuk namespace Anda)
            $namespace = Uuid::uuid5(Uuid::NAMESPACE_DNS, 'idpengguna');
        
            // Menghasilkan UUID versi 5 berdasarkan namespace dan nama (nilai tertentu)
            return Uuid::uuid5($namespace, $name)->toString();
        
        }
    }
    
    if (!function_exists('limitText')) {
        function limitText($text, $limit) {
            if (strlen($text) > $limit) {
                return substr($text, 0, $limit) . '...';
            } else {
                return $text;
            }
        }
    }
    
    if (!function_exists('menuActive')) {
        function menuActive($valeuMenu, $namaMenu)
        {
            if ($valeuMenu == $namaMenu) {
                return 'active';
            }
        }
    }

    
    if (!function_exists('statusKegiatan')) {
        function statusKegiatan($status)
        {
            if ($status == 'dibuka') {
                return '<span class="badge bg-label-success me-1">Dibuka</span>';
            }
            elseif ($status == 'ditutup') {
                return '<span class="badge bg-label-danger me-1">Ditutup</span>';
            }
            else{
                return '<span class="badge bg-label-secondary me-1">'. ucfirst($status) .'</span>';
            }
        }
    }

    if (!function_exists('statusPeserta')) {
        function statusPeserta($status = NULL)
        {
            if ($status == 'diterima') {
                return '<span class="badge bg-label-success me-1">Diterima</span>';
            }
            elseif ($status == 'tidak-diterima') {
                return '<span class="badge bg-label-danger me-1">Ditolak</span>';
            }
            else{
                return '<span class="badge bg-label-primary me-1">Menunggu</span>';
            }
        }
    }
    
    
    if (!function_exists('formatWaktu')) {
        function formatWaktu($waktu) {
            $waktuSekarang = time();
            $selisihDetik = $waktuSekarang - strtotime($waktu);
        
            // Kurang dari 1 menit
            if ($selisihDetik < 60) {
                return 'Baru saja';
            }
        
            // Kurang dari 1 jam
            if ($selisihDetik < 3600) {
                $menit = floor($selisihDetik / 60);
                return $menit . ' menit lalu';
            }
        
            // Kurang dari 24 jam
            if ($selisihDetik < 86400) {
                $jam = floor($selisihDetik / 3600);
                return $jam . ' jam lalu';
            }
        
            // Kurang dari 7 hari
            if ($selisihDetik < 604800) {
                $hari = floor($selisihDetik / 86400);
                return $hari . ' hari lalu';
            }
        
            // Melebihi kondisi di atas
            return date('H:i d F Y', strtotime($waktu));
        }
    }
    
    if (!function_exists('tindakanPeserta')) {
        function tindakanPeserta($reg_id, $status) {
            
            if ($status == NULL) {
                return '<a href="javascript:void(0);" onclick="terimaPeserta(\''. $reg_id .'\')" class="btn btn-sm btn-icon btn-label-success" >
                            <i class="fas fa-check"></i>
                        </a>
                        <a href="javascript:void(0);" onclick="tolakPeserta(\''. $reg_id .'\')" class="btn btn-sm btn-icon btn-label-danger" >
                            <i class="fas fa-times"></i>
                        </a>';
            }
            else{
                return '<a href="javascript:void(0);" onclick="hapusPesertaKegiatan(\''. $reg_id .'\')" class="btn btn-sm btn-icon btn-label-dark" >
                            <i class="fas fa-trash"></i>
                        </a>';
            }
        }
    }

    