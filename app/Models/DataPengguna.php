<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class DataPengguna extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    protected $table      = 'data_pengguna';
    protected $primaryKey = 'pgnid';
    protected $keyType    = 'string';
    public $incrementing  = false;

    protected $fillable = [
        'pgnid',
        'email',
        'password',
        'nama_lengkap',
        'nomor_hp',
        'jabatan',
        'level_akun',
        'status_akun',
        'foto_profile'
    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];
}
