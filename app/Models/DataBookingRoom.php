<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DataBookingRoom extends Model
{
    use HasFactory;

    protected $table      = 'data_bookingroom';
    protected $primaryKey = 'bookid';
    public $incrementing  = false;
    protected $keyType    = 'string';

    protected $fillable = [
        'bookid',
        'ruanganid',
        'penggunaid',
        'tanggal_mulai',
        'tanggal_selesai',
        'waktu_mulai',
        'waktu_selesai',
        'tujuan_kegiatanbook',
        'catatan'
    ];

    public function ruangan()
    {
        return $this->belongsTo(DataRuangan::class, 'ruanganid', 'rgnid');
    }

    public function pengguna()
    {
        return $this->belongsTo(DataPengguna::class, 'penggunaid', 'pgnid');
    }
}
