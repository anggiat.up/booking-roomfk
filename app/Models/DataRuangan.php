<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DataRuangan extends Model
{
    use HasFactory;

    protected $table      = 'data_ruangan';
    protected $primaryKey = 'rgnid';
    protected $keyType    = 'string';
    public $incrementing  = false;

    protected $fillable = [
        'rgnid',
        'nama_ruangan',
        'warna_ruangan',
        'status_ruangan',
        'jumlah_kursi',
        'keterangan',
        'fasilitas_lain',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];
}
