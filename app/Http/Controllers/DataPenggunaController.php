<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\DataPengguna;

class DataPenggunaController extends Controller
{
    // Halaman Data Pengguna
    public function pageDataPengguna()
    {
        $page = [
            'judulhalaman' => 'pengguna',
            'nama_halaman' => 'Data Pengguna',
            'appname'      => config('app.name')
        ];
        return view('pages/data-pengguna', $page);
    }




    // =============
    // Area req AJAX



    // Ajax Method Users

    // Ajax Datatable Users
    public function reqAjaxdataPengguna()
    {
        if (request()->ajax()) 
        {
            return Datatables()->of(DataPengguna::all())
                ->addIndexColumn()
                ->editColumn('level_akun', function($row){
                    return formatLevel($row->level_akun);
                })
                ->editColumn('aksi', function($row){
                    return '<a href="javascript:void(0);" onclick="detailPengguna(\''. $row->pgnid .'\')" class="btn btn btn-icon btn-label-primary" >
                                <i class="fas fa-info-circle"></i>
                            </a>
                            <a href="javascript:void(0);" onclick="updatePengguna(\''. $row->pgnid .'\')" class="btn btn btn-icon btn-label-warning" >
                                <i class="fas fa-user-edit"></i>
                            </a>';
                })
                ->rawColumns(['pgnid','nama_lengkap', 'email', 'status_akun', 'level_akun','aksi'])
                ->make(true);
        }
    }

    // Ajax Detail User
    public function ajaxDetailPengguna(Request $request)
    {
        if (request()->ajax()) 
        {
            $cari      = ['pgnid' => $request->pengguna];
            $datausers = DataPengguna::where($cari)->first();
            
            if ($datausers) {
                $data      = [
                                'pgnid'        => $datausers->pgnid,
                                'email'        => $datausers->email,
                                'nama_lengkap' => $datausers->nama_lengkap,
                                'status_akun'  => $datausers->status_akun,
                                'level_akun'   => $datausers->level_akun,
                                'jabatan'      => $datausers->jabatan,
                                'nomor_hp'     => $datausers->nomor_hp
                            ];
                         
                $data['flevel_akun'] = formatLevel($datausers['level_akun']);
                return response()->json($data);
            }
            else{
                return response()->json(['statuslog'=> 'error' ,'message' => 'Data tidak ditemukan']);
            }
        }
    }

    // Ajax Update User
    public function ajaxEditPengguna(Request $request)
    {
        if (request()->ajax()) 
        {
            $nama_lengkap  = $request->nama_lengkap;
            $email         = $request->email;
            $no_hp         = $request->no_hp;
            $level_akun    = $request->level_akun;
            $status_akun   = $request->status_akun;
            $pengguna_id   = $request->pengguna_id;

            $updatepengguna = [
                                'nama_lengkap'  => $nama_lengkap,
                                'email'         => $email,
                                'nomor_hp'      => $no_hp,
                                'level_akun'    => $level_akun,
                                'status_akun'   => $status_akun,
                              ];

            if (DataPengguna::where('email', $email)->where('pgnid', '!=', $pengguna_id)->exists()) {
                return response()->json(['statuslog' => 'error','title' => 'GAGAL','message'=> 'Email telah digunakan pada akun lain']);
            }
            else{
                try {
                        DB::beginTransaction();
                            DataPengguna::where(['pgnid' => $pengguna_id])->update($updatepengguna);
                        DB::commit();

                        return response()->json(['statuslog' => 'success', 'message' => 'Update data pengguna berhasil dilakukan']);
                    } 
                    catch (\Exception $e) 
                    {
                        DB::rollback();
                        return response()->json(['statuslog' => 'error', 'message' => 'Update data pengguna gagal dilakukan']);
                    }                
            }
            
        }
    }
}
