<?php

namespace App\Http\Controllers;

use App\Models\DataPengguna;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Ramsey\Uuid\Uuid;

class AuthsController extends Controller
{

    public function pageLogin()
    {
        if (Auth::check()) {
            return redirect('/home');
        }

        $page = [
            'nama_halaman' => 'Login',
            'appname'      => config('app.name')

        ];
        return view('layout/page-login', $page);
    }

    // Method Ajax Login
    public function AjaxprosesLogin(Request $request)
    {
        $request->validate([
            'email'    => ['required','email'],
            'password' => 'required'
        ]);

        // Credentials api
        $credentials =    [
            'email'  => $request->email,
            'pass'   => $request->password
        ];
        
        // Credentials local storage
        $credentialslocal =    [
            'email'     => $request->email,
            'password'  => $request->password
        ];

        $user = DataPengguna::where('email', $request->email)->first();
        // Jika email ditemukan di db local
        if ($user) 
        {
            // Login pengguna ke aplikasi lokal, email dan pw yang disimpan dilocal
            if (Auth::attempt($credentialslocal)) {
                // Jika login berhasil
                // Status akun aktif
                if (auth()->user()->status_akun === 'aktif') {                    
                    return response()->json(['statuslog' => 'success', 'message' => 'Login Berhasil', 'links' =>'home']);
                } 
                // Status akun tidak aktif
                else {
                    Auth::logout(); // Logout pengguna yang status akunnya tidak aktif
                    return response()->json(['statuslog' => 'error', 'message' => 'Akun Anda tidak aktif. Hubungi administrator.'], 403);
                }
            }
            // Login pengguna melalui api aorta, email dan pw dari aorta
            else {
                $response = $this->loginApiAorta($credentials);
                return $response;
            }
        }
        // Jika email tidak ditemukan di db local
        else {
            $response = $this->loginApiAorta($credentials);
            return $response;
        }

    }

    // Method  Logout
    public function logout(Request $request)
    {
        Auth::logout(); 
        $request->session()->invalidate();

        return redirect('');
    }

    private function loginApiAorta($credentials)
    {
        $client     = new Client();
        try {
            // Kirim permintaan ke API eksternal
            $response = $client->post('https://public.aorta.systems/api/login', [
                'form_params' => $credentials,
                'timeout'     => 10, // Timeout dalam detik
            ]);        

            $apiResponse = json_decode($response->getBody(), true);            
            // Jika login berhasil
            if ($apiResponse['status']) 
            {
                $user = DataPengguna::where('email', $credentials['email'])->first();
                if (!$user) {
                    // Jika belum ada, simpan data pengguna ke database lokal                    
                    $user = DataPengguna::create([
                        'pgnid'        => generateUuid($apiResponse['id_user']),
                        'email'        => $credentials['email'],
                        'password'     => bcrypt($credentials['pass']), // Enkripsi password
                        'nama_lengkap' => $apiResponse['nama_user'],
                        'level_akun'   => 'pgn',
                        'status_akun'  => 'aktif',
                        'created_at'   => now(),
                        'updated_at'   => now(),
                    ]);     
                    // Login pengguna ke aplikasi lokal
                    Auth::login($user);
                    return response()->json(['statuslog' => 'success', 'message' => 'Login Berhasil', 'links' =>'home']);
                }      
                // Jika sudah ada sinkronkan password local dengan password api     
                else{
                    $user->update([
                        'password'     => bcrypt($credentials['password']), // Enkripsi password
                        'updated_at'   => now(),
                    ]);
                    // Login pengguna ke aplikasi lokal
                    Auth::login($user);
                    return response()->json(['statuslog' => 'success', 'message' => 'Login Berhasil', 'links' =>'home']);
                }
            } 
            // Jika login gagal
            else {
                // Jika login ke API eksternal gagal
                return response()->json(['statuslog' => 'error', 'message' => 'Email atau Password yang Anda inputkan salah'], 404);
            }
        } catch (\Exception $e) {
            // Tangani kesalahan
            return response()->json(['statuslog' => 'error', 'message' => 'Terjadi kesalahan. Silakan coba lagi.'], 500);
        }
    }


    
}
