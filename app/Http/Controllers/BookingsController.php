<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\DataBookingRoom;
use Illuminate\Http\Request;
use App\Models\DataPengguna;
use App\Models\DataRuangan;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Ramsey\Uuid\Uuid;
use DateTime;
use Illuminate\Support\Facades\Auth;

class BookingsController extends Controller
{
    public function pageDashboard()
    {
        $dataruangan = DataRuangan::orderBy('nama_ruangan', 'asc')->get();
        $databooking = DataBookingRoom::with('ruangan')->with('pengguna')->limit(5)->orderBy('created_at', 'desc')->get();
        $page = [
            'judulhalaman' => 'dashboard',
            'nama_halaman' => 'Dashboard',
            'appname'      => config('app.name'),
            'dataruangan'  => $dataruangan,
            'databooking'  => $databooking,

        ];
        return view('pages/dashboard', $page);        
    }

    public function pageKalender()
    {
        $page = [
            'judulhalaman' => 'kalender',
            'nama_halaman' => 'Kalender',
            'appname'      => config('app.name')

        ];
        return view('pages/kalender', $page);
    }

    public function pageBookingRoom()
    {
        $dataruangan = DataRuangan::orderBy('jumlah_kursi', 'asc')->get();
        $page = [
            'judulhalaman' => 'bookingroom',
            'nama_halaman' => 'Data Booking Room',
            'appname'      => config('app.name'),
            'dataruangan'  => $dataruangan,

        ];
        return view('pages/data-bookingroom', $page);
    }


    public function reqAjaxTableBookingRoom()
    {
        if (request()->ajax()) 
        {
            return Datatables()->of(DataBookingRoom::with('ruangan')->with('pengguna')->orderBy('created_at', 'desc')->get())
                ->addIndexColumn()
                ->editColumn('nama_ruangan', function($row){
                    return $row->ruangan->nama_ruangan;
                })
                ->editColumn('dibooking', function($row){
                    return $row->pengguna->nama_lengkap;
                })
                ->editColumn('rentang_tanggal', function($row){
                    if (date('d', strtotime($row->tanggal_mulai)) == date('d', strtotime($row->tanggal_selesai))) {
                        return date('d/M/Y', strtotime($row->tanggal_selesai));
                    } 
                    else {
                        return date('d', strtotime($row->tanggal_mulai)) . '-' . date('d/M/Y', strtotime($row->tanggal_selesai));
                    }
                    
                })
                ->editColumn('rentang_waktu', function($row){
                    return $row->waktu_mulai . ' sd ' . $row->waktu_selesai;
                })
                ->editColumn('tujuan_kegiatanbook', function($row){
                    return limitText($row->tujuan_kegiatanbook, 25);
                })
                ->editColumn('aksi', function($row){

                    $button = NULL;

                    if (Auth::check()) 
                    {
                        if (($row->penggunaid == auth()->user()->pgnid) || auth()->user()->level_akun == 'adm') { 
                            $button = '<button type="button" data-booking="'. $row->bookid .'" class="btn btn btn-icon btn-label-warning btn-edit-booking " >
                                            <i class="fas fa-user-edit"></i>
                                        </button>
                                        <button type="button" data-booking="'. $row->bookid .'" class="btn btn btn-icon btn-label-danger btn-hapus-booking m-1" >
                                            <i class="fas fa-trash"></i>
                                        </button>';
                        }
                    }
                    
                    $button .= '<button type="button" data-booking="'. $row->bookid .'" class="btn btn btn-icon btn-label-primary btn-detail-booking" >
                                <i class="fas fa-info-circle"></i>
                            </button>';

                    return $button;
                })
                ->rawColumns(['aksi'])
                ->make(true);
        }
    }

    public function reqAjaxDataBookingCalendar()
    {
        $today         = Carbon::today();
        $awalBulanIni  = Carbon::now()->subMonths(6)->startOfMonth();
        $akhirBulanIni = Carbon::now()->addMonths(6)->endOfMonth();

        // Query untuk mengambil data booking room dengan tanggal mulai antara awal dan akhir bulan ini
        $data_booking = DataBookingRoom::with(['ruangan','pengguna'])->whereBetween('tanggal_mulai', [$awalBulanIni, $akhirBulanIni])->get()->toArray();
        $newformat    = [];
        
        foreach ($data_booking as $key => $value) 
        {   
            
            $rentang_tanggal = NULL;
            if (date('d', strtotime($value['tanggal_mulai'])) == date('d', strtotime($value['tanggal_selesai']))) {
                $rentang_tanggal = date('d, M Y', strtotime($value['tanggal_selesai']));
            } 
            else {
                $rentang_tanggal = date('d', strtotime($value['tanggal_mulai'])) . ' - ' . date(' d, M Y', strtotime($value['tanggal_selesai']));
            }

            // (date('d', strtotime($value['tanggal_mulai'])) == date('d', strtotime($value['tanggal_selesai']))) ? date('d/M/Y', strtotime($value['tanggal_selesai'])) : date('d', strtotime($value['tanggal_mulai'])) . '-' . date('d/M/Y', strtotime($value['tanggal_selesai']));
            $newformat[$key]['title']               = $value['tujuan_kegiatanbook'];
            $newformat[$key]['location']            = $value['ruangan']['nama_ruangan'];
            $newformat[$key]['catatan']             = $value['catatan'];
            $newformat[$key]['booking']             = $value['pengguna']['nama_lengkap'];
            $newformat[$key]['tanggal']             = $rentang_tanggal;
            $newformat[$key]['start']               = date('Y-m-d', strtotime($value['tanggal_mulai'])) . 'T' . date('H:i:s', strtotime($value['waktu_mulai']));
            $newformat[$key]['end']                 = date('Y-m-d', strtotime($value['tanggal_selesai'])) . 'T' . date('H:i:s', strtotime($value['waktu_selesai']));
            $newformat[$key]['backgroundColor']     = $value['ruangan']['warna_ruangan']; // Mengakses warna_ruangan dari relasi
            // $newformat[$key]['extendedProps']   = ['calendar' => 'Family'];
            $newformat[$key]['textColor']     = checkBrightness($value['ruangan']['warna_ruangan']); // Mengakses warna_ruangan dari relasi

            

        }
        
        return response()->json($newformat);
    }

    public function ajaxFormTambahBooking(Request $request)
    {
        if (request()->ajax()) 
        {
            $pengguna        = $request->pengguna;
            $nama_kegiatan   = $request->nama_kegiatan;
            $nama_ruangan    = $request->nama_ruangan;
            $rentang_tanggal = $request->rentang_tanggal;
            $waktu_mulai     = $request->waktu_mulai;
            $durasi          = $request->durasi;
            $catatan         = $request->catatan;

            if (empty($catatan)) {
                $catatan = '-';
            }

            $tanggal_awal    = getRentang($rentang_tanggal, 'awal');
            $tanggal_akhir   = getRentang($rentang_tanggal, 'akhir');

            $NewBookstartDate = new DateTime($tanggal_awal);
            $NewBookendDate   = new DateTime($tanggal_akhir); 
            $newBooknumDays   = $NewBookstartDate->diff($NewBookendDate)->days + 1;

            

            // Mendapatkan jam mulai dan jam akhir berdasarkan waktu mulai dan durasi
            $jam_mulai = Carbon::createFromFormat('H:i', $waktu_mulai)->format('H:i');
            $jam_akhir = Carbon::createFromFormat('H:i', $waktu_mulai)->addMinutes($durasi)->format('H:i');

            $conflictingBooking = NULL;

            $checkTanggalBook = DataBookingRoom::where(function ($query) use ($tanggal_awal, $tanggal_akhir) {
                $query->where('tanggal_mulai', '<=', $tanggal_akhir) // Tanggal mulai kurang dari atau sama dengan tanggal yang diberikan
                      ->where('tanggal_selesai', '>=', $tanggal_awal); // Tanggal selesai lebih dari atau sama dengan tanggal yang diberikan
            })
            ->where('ruanganid', $nama_ruangan)
            ->get();

            // dd($checkTanggalBook);
            // die;

            if ($checkTanggalBook) 
            {
                // Looping data booked yang ada didalam sistem
                foreach ($checkTanggalBook as $booking) {

                    // Lakukan pengecekan tanggal berulang berdasarkan jumlah hari dari bookingan terbaru
                    for ($nb=0; $nb < $newBooknumDays; $nb++) 
                    { 
                        // Inisiasi format tanggal booking terbaru
                        $newBookRangeDates = clone $NewBookstartDate;
                        $newBookRangeDates->modify("+$nb days");
                        $newBookWaktuMulai   = $newBookRangeDates->format('d-m-Y ') . $jam_mulai;
                        $newBookWaktuSelesai = $newBookRangeDates->format('d-m-Y ') . $jam_akhir;


                        // Inisiasi format tanggal dari bookingan yang telah ada
                        $startDate = new DateTime($booking->tanggal_mulai);
                        $endDate   = new DateTime($booking->tanggal_selesai);
                        $numDays   = $startDate->diff($endDate)->days + 1;                        

                        // Lakukan pengecekan ulang berdasarkan jumlah hari dari  bookingan yang telah ada
                        for ($i=0; $i < $numDays ; $i++) { 
                            $currentDate = clone $startDate; // Clone objek tanggal mulai agar tidak berubah nilainya
                            $currentDate->modify("+$i days"); // Tambahkan $i hari ke tanggal mulai
                        
                            // Format tanggal tanggal dan waktu jam
                            $bookWaktuMulai   = $currentDate->format('d-M-Y ') . $booking->waktu_mulai; 
                            $bookWaktuSelesai = $currentDate->format('d-M-Y ') . $booking->waktu_selesai; 
                            
                            // Convert waktu ke int
                            $oldbooktgl_mulai        = strtotime($bookWaktuMulai);
                            $oldbooktgl_selesai      = strtotime($bookWaktuSelesai);

                            $waktu_yang_dicekawal    = strtotime($newBookWaktuMulai);
                            $waktu_yang_dicekakhir   = strtotime($newBookWaktuSelesai);

                            // echo $oldbooktgl_mulai . ' >= ' . $waktu_yang_dicekakhir . ' && ' . $oldbooktgl_selesai . ' <= ' . $waktu_yang_dicekawal  . '<br><br>';
                            
                            if ($waktu_yang_dicekawal < $oldbooktgl_selesai && $waktu_yang_dicekakhir > $oldbooktgl_mulai) {
                                $conflictingBooking = TRUE;
                            }                            

                        }
                    }
                    
                }

            }
            
            $data_bookingroom = [
                                    'bookid'              => Uuid::uuid4(),
                                    'ruanganid'           => $nama_ruangan,
                                    'penggunaid'          => $pengguna,
                                    'tanggal_mulai'       => $tanggal_awal,
                                    'tanggal_selesai'     => $tanggal_akhir,
                                    'waktu_mulai'         => $jam_mulai,
                                    'waktu_selesai'       => $jam_akhir,
                                    'tujuan_kegiatanbook' => $nama_kegiatan,
                                    'catatan'             => $catatan,
                                    'created_at'          => now(),
                                    'updated_at'          => now(),
                                ];
            
            
                                
            
            if ($conflictingBooking) {                
                return response()->json(['statuslog' => 'error','title' => 'GAGAL','message'=> 'Waktu dan tanggal Ruangan yang anda pilih telah dibooking.']);
            }
            else{
                try {
                    DB::beginTransaction();
                        DataBookingRoom::insert($data_bookingroom);
                    DB::commit();
                    return response()->json(['statuslog' => 'success', 'message' => 'Berhasil melakukan booking room!']);
                } 
                catch (\Exception $e) 
                {
                    DB::rollback();
                    return response()->json(['statuslog' => 'error', 'message' => 'Booking room gagal dilakukan!']);
                }   
            }
            
            


        }
    }

    public function reqajaxDetailBooking(Request $request)
    {
        if (request()->ajax()) 
        {
            $cari        = ['bookid' => $request->booking];
            $databooking = DataBookingRoom::with('ruangan')->with('pengguna')->where($cari)->first();
            
            if ($databooking) {
                $data      = [
                                'bookid'              => $databooking->bookid,
                                'ruanganid'           => $databooking->ruanganid,
                                'nama_ruangan'        => $databooking->ruangan->nama_ruangan,
                                'penggunaid'          => $databooking->penggunaid,
                                'nama_lengkap'        => $databooking->pengguna->nama_lengkap,
                                'tanggal_mulai'       => $databooking->tanggal_mulai,
                                'tanggal_selesai'     => $databooking->tanggal_selesai,
                                'waktu_mulai'         => $databooking->waktu_mulai,
                                'waktu_selesai'       => $databooking->waktu_selesai,
                                'tujuan_kegiatanbook' => $databooking->tujuan_kegiatanbook,
                                'catatan'             => $databooking->catatan,
                                'created_at'          => $databooking->created_at,
                                'updated_at'          => $databooking->updated_at,
                                'durasi'              => (strtotime($databooking->tanggal_mulai . ' ' .  $databooking->waktu_selesai) - strtotime($databooking->tanggal_mulai . ' ' . $databooking->waktu_mulai)) / 60, 
                                'waktu'               => date('d-F-Y', strtotime($databooking->tanggal_mulai)) . ' sd ' . date('d-F-Y', strtotime($databooking->tanggal_selesai)), 
                                'jam'                 => $databooking->waktu_mulai . ' sd ' . $databooking->waktu_selesai
                             ];

                return response()->json($data);
            }
            else{
                return response()->json(['statuslog'=> 'error' ,'message' => 'Data ruangan tidak ditemukan']);
            }
        }
    }

    public function ajaxFormEditBooking(Request $request)
    {
        if (request()->ajax()) 
        {
            $pengguna        = $request->pengguna;
            $pengguna        = $request->pengguna;
            $nama_kegiatan   = $request->nama_kegiatan;
            $nama_ruangan    = $request->nama_ruangan;
            $rentang_tanggal = $request->rentang_tanggal;
            $waktu_mulai     = $request->waktu_mulai;
            $durasi          = $request->durasi;
            $catatan         = $request->catatan;
            $bookid          = $request->bookingid;

            if (empty($catatan)) {
                $catatan = '-';
            }

            $tanggal_awal    = getRentang($rentang_tanggal, 'awal');
            $tanggal_akhir   = getRentang($rentang_tanggal, 'akhir');

            $NewBookstartDate = new DateTime($tanggal_awal);
            $NewBookendDate   = new DateTime($tanggal_akhir); 
            $newBooknumDays   = $NewBookstartDate->diff($NewBookendDate)->days + 1;

            // Mendapatkan jam mulai dan jam akhir berdasarkan waktu mulai dan durasi
            $jam_mulai = Carbon::createFromFormat('H:i', $waktu_mulai)->format('H:i');
            $jam_akhir = Carbon::createFromFormat('H:i', $waktu_mulai)->addMinutes($durasi)->format('H:i');

            $conflictingBooking = NULL;

            $checkTanggalBook = DataBookingRoom::where(function ($query) use ($tanggal_awal, $tanggal_akhir) {
                $query->where('tanggal_mulai', '<=', $tanggal_akhir) // Tanggal mulai kurang dari atau sama dengan tanggal yang diberikan
                      ->where('tanggal_selesai', '>=', $tanggal_awal); // Tanggal selesai lebih dari atau sama dengan tanggal yang diberikan
            })
            ->where('ruanganid', $nama_ruangan)
            ->where('bookid', '!=', $bookid)
            ->get();

            if ($checkTanggalBook) 
            {
                // Looping data booked yang ada didalam sistem
                foreach ($checkTanggalBook as $booking) {

                    // Lakukan pengecekan tanggal berulang berdasarkan jumlah hari dari bookingan terbaru
                    for ($nb=0; $nb < $newBooknumDays; $nb++) 
                    { 
                        // Inisiasi format tanggal booking terbaru
                        $newBookRangeDates = clone $NewBookstartDate;
                        $newBookRangeDates->modify("+$nb days");
                        $newBookWaktuMulai   = $newBookRangeDates->format('d-m-Y ') . $jam_mulai;
                        $newBookWaktuSelesai = $newBookRangeDates->format('d-m-Y ') . $jam_akhir;

                        // Inisiasi format tanggal dari bookingan yang telah ada
                        $startDate = new DateTime($booking->tanggal_mulai);
                        $endDate   = new DateTime($booking->tanggal_selesai);
                        $numDays   = $startDate->diff($endDate)->days + 1;                        

                        // Lakukan pengecekan ulang berdasarkan jumlah hari dari  bookingan yang telah ada
                        for ($i=0; $i < $numDays ; $i++) { 
                            $currentDate = clone $startDate; // Clone objek tanggal mulai agar tidak berubah nilainya
                            $currentDate->modify("+$i days"); // Tambahkan $i hari ke tanggal mulai
                        

                            // Format tanggal tanggal dan waktu jam
                            $bookWaktuMulai   = $currentDate->format('d-M-Y ') . $booking->waktu_mulai; 
                            $bookWaktuSelesai = $currentDate->format('d-M-Y ') . $booking->waktu_selesai; 
                            
                            // Convert waktu ke int
                            $oldbooktgl_mulai        = strtotime($bookWaktuMulai);
                            $oldbooktgl_selesai      = strtotime($bookWaktuSelesai);

                            $waktu_yang_dicekawal    = strtotime($newBookWaktuMulai);
                            $waktu_yang_dicekakhir   = strtotime($newBookWaktuSelesai);

                            // echo $oldbooktgl_mulai . ' >= ' . $waktu_yang_dicekakhir . ' && ' . $oldbooktgl_selesai . ' <= ' . $waktu_yang_dicekawal  . '<br><br>';
                            
                            if ($waktu_yang_dicekawal < $oldbooktgl_selesai && $waktu_yang_dicekakhir > $oldbooktgl_mulai) {
                                $conflictingBooking = TRUE;
                            }              
                        }
                    }
                    
                }
            }
            
            $data_bookingroom = [
                                    'ruanganid'           => $nama_ruangan,
                                    'tanggal_mulai'       => $tanggal_awal,
                                    'tanggal_selesai'     => $tanggal_akhir,
                                    'waktu_mulai'         => $jam_mulai,
                                    'waktu_selesai'       => $jam_akhir,
                                    'tujuan_kegiatanbook' => $nama_kegiatan,
                                    'catatan'             => $catatan,
                                    'updated_at'          => now(),
                                ];
            
            
                                
            
            if ($conflictingBooking) {                
                return response()->json(['statuslog' => 'error','title' => 'GAGAL','message'=> 'Waktu dan tanggal Ruangan yang anda pilih telah dibooking.']);
            }
            else{
                try {
                    DB::beginTransaction();
                        DataBookingRoom::where(['bookid' => $bookid])->update($data_bookingroom);

                    DB::commit();
                    return response()->json(['statuslog' => 'success', 'message' => 'Berhasil melakukan perubahan booking room']);
                } 
                catch (\Exception $e) 
                {
                    DB::rollback();
                    return response()->json(['statuslog' => 'error', 'message' => 'Update Booking room gagal dilakukan!']);
                }   
            }
            
            


        }
    }

    public function reqAjaxHapusBooking(Request $request)
    {
        if (request()->ajax()) 
        {
            $bookid           = $request->booking;
            try {
                DB::beginTransaction();
                    DataBookingRoom::where(['bookid' => $bookid])->delete();
                DB::commit();
                return response()->json(['statuslog' => 'success', 'message' => 'Booking Room berhasil dihapus', 'title' => 'BERHASIL']);
            } 
            catch (\Exception $e) 
            {
                DB::rollback();
                return response()->json(['statuslog' => 'error', 'message' => 'Gagal menghapus data Booking Room, mohon coba kembali', 'title' => 'GAGAL']);
            }
        }
        else{
            return response()->json(['statuslog'=> 'error' ,'message' => 'Gagal Mengakses Halaman'], 404);
        }
    }

    public function reqApiDataBookingRoom(Request $request)
    {
        $limitDays = $request->input('days', 5); // Default 30 hari jika tidak ada parameter
        $today     = date('Y-m-d'); // Menggunakan format Y-m-d secara langsung

        $bookingRooms = DataBookingRoom::with([
            'ruangan' => function($query) {
                $query->select('rgnid', 'nama_ruangan');
            },
            'pengguna' => function($query) {
                $query->select('pgnid', 'nama_lengkap');
            }
        ])
        ->select('tujuan_kegiatanbook', 'waktu_mulai', 'ruanganid', 'tanggal_mulai')
        ->whereDate('tanggal_mulai', '>=', $today)  // Ambil semua data dari tanggal hari ini ke depan
        ->whereDate('tanggal_mulai', '<=', date('Y-m-d', strtotime($today. ' + '.$limitDays.' days')))
        ->get();
    
        // Mengubah nama field dalam hasil menjadi format yang diinginkan
        $formattedResults = $bookingRooms->map(function($bookingRoom) {
            return [
                'judul_kegiatan' => $bookingRoom->tujuan_kegiatanbook,
                'lokasi' => $bookingRoom->ruangan->nama_ruangan,
                'waktu' => $bookingRoom->waktu_mulai . ' WIB, ' .  date('d-M-Y', strtotime($bookingRoom->tanggal_mulai)),
            ];
        });

        return response()->json($formattedResults);
    }

}
