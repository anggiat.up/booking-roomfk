<?php

namespace App\Http\Controllers;

use App\Models\DataRuangan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Ramsey\Uuid\Uuid;

class RuanganController extends Controller
{
    public function pageRuangan()
    {
        $ruangan = DataRuangan::orderBy('nama_ruangan', 'ASC');
        $page = [
            'judulhalaman' => 'ruangan',
            'nama_halaman' => 'Data Ruangan',
            'appname'      => config('app.name'),
            'dataruangan'  => $ruangan,

        ];
        return view('pages/data-ruangan', $page);        
    }




    public function reqAjaxdataRuangan()
    {
        if (request()->ajax()) 
        {
            return Datatables()->of(DataRuangan::all())
                ->addIndexColumn()
                ->editColumn('status_ruangan', function($row){
                    return $row->status_ruangan;
                })
                ->editColumn('warna_ruangan', function($row){
                    return '<div class="" style="width: 70px;height:30px;margin: auto;background-color:'. $row->warna_ruangan .'"></div>';
                })
                ->editColumn('aksi', function($row){
                    return '<button type="button" data-ruangan="'. $row->rgnid .'" class="btn btn btn-icon btn-label-warning btn-edit-ruangan" >
                                <i class="fas fa-user-edit"></i>
                            </button>
                            <button type="button" data-ruangan="'. $row->rgnid .'" class="btn btn btn-icon btn-label-primary btn-detail-ruangan" >
                                <i class="fas fa-info-circle"></i>
                            </button>';
                })
                ->rawColumns(['status_ruangan', 'warna_ruangan','aksi'])
                ->make(true);
        }
    }

    public function ajaxTambahRuangan(Request $request)
    {
        if (request()->ajax()) 
        {
            $nama_ruangan   = $request->nama_ruangan;
            $status_ruangan = $request->status_ruangan;
            $jumlah_kursi   = $request->jumlah_kursi;
            $fasilitas      = $request->fasilitas;
            $warna_ruangan  = $request->warna_ruangan;
            $keterangan     = $request->keterangan;

            $data_ruangan = [
                                'rgnid'          => Uuid::uuid4(),
                                'nama_ruangan'   => $nama_ruangan,
                                'status_ruangan' => $status_ruangan,
                                'jumlah_kursi'   => $jumlah_kursi,
                                'fasilitas_lain' => $fasilitas,
                                'warna_ruangan'  => $warna_ruangan,
                                'keterangan'     => $keterangan,
                                'created_at'     => now(),
                                'updated_at'     => now()
                              ];

            if (DataRuangan::where('nama_ruangan', $nama_ruangan)->exists()) {
                return response()->json(['statuslog' => 'error','title' => 'GAGAL','message'=> 'Nama Ruangan telah digunakan sebelumnya']);
            }
            elseif (DataRuangan::where('warna_ruangan', $warna_ruangan)->exists()) {
                return response()->json(['statuslog' => 'error','title' => 'GAGAL','message'=> 'Nama Ruangan telah digunakan sebelumnya']);
            }
            else{
                try {
                    DB::beginTransaction();
                        DataRuangan::insert($data_ruangan);
                    DB::commit();

                        return response()->json(['statuslog' => 'success', 'message' => 'Berhasil menambahkan data Ruangan baru!']);
                    } 
                    catch (\Exception $e) 
                    {
                        DB::rollback();
                        return response()->json(['statuslog' => 'error', 'message' => 'Data Ruangan gagal ditambahkan!']);
                    }                
            }
            
        }
    }

    public function ajaxDetailRuangan(Request $request)
    {
        if (request()->ajax()) 
        {
            $cari        = ['rgnid' => $request->ruangan];
            $dataruangan = DataRuangan::where($cari)->first();
            
            if ($dataruangan) {
                $data      = [
                                'rgnid'          => $dataruangan->rgnid,
                                'nama_ruangan'   => $dataruangan->nama_ruangan,
                                'warna_ruangan'  => $dataruangan->warna_ruangan,
                                'status_ruangan' => $dataruangan->status_ruangan,
                                'jumlah_kursi'   => $dataruangan->jumlah_kursi,
                                'keterangan'     => $dataruangan->keterangan,
                                'fasilitas_lain' => $dataruangan->fasilitas_lain
                            ];
                         
                return response()->json($data);
            }
            else{
                return response()->json(['statuslog'=> 'error' ,'message' => 'Data ruangan tidak ditemukan']);
            }
        }
    }

    public function ajaxUpdateRuangan(Request $request)
    {
        if (request()->ajax()) 
        {
            $ruanganid      = $request->ruanganid;
            $nama_ruangan   = $request->nama_ruangan;
            $status_ruangan = $request->status_ruangan;
            $jumlah_kursi   = $request->jumlah_kursi;
            $fasilitas      = $request->fasilitas;
            $warna_ruangan  = $request->warna_ruangan;
            $keterangan     = $request->keterangan;

            $data_ruangan = [
                                'nama_ruangan'   => $nama_ruangan,
                                'status_ruangan' => $status_ruangan,
                                'jumlah_kursi'   => $jumlah_kursi,
                                'fasilitas_lain' => $fasilitas,
                                'warna_ruangan'  => $warna_ruangan,
                                'keterangan'     => $keterangan,
                                'updated_at'     => now()
                              ];

            
            if (DataRuangan::where('nama_ruangan', $nama_ruangan)->where('rgnid', '!=', $ruanganid)->exists()) {
                return response()->json(['statuslog' => 'error','title' => 'GAGAL','message'=> 'Nama Ruangan telah digunakan sebelumnya']);
            }
            elseif (DataRuangan::where('warna_ruangan', $warna_ruangan)->where('rgnid', '!=', $ruanganid)->exists()) {
                return response()->json(['statuslog' => 'error','title' => 'GAGAL','message'=> 'Warna Ruangan telah digunakan sebelumnya']);
            }
            else{
                try {
                    DB::beginTransaction();
                        DataRuangan::where(['rgnid' => $ruanganid])->update($data_ruangan);

                    DB::commit();

                        return response()->json(['statuslog' => 'success', 'message' => 'Update data Ruangan berhasil!']);
                    } 
                    catch (\Exception $e) 
                    {
                        DB::rollback();
                        return response()->json(['statuslog' => 'error', 'message' => 'Gagal melakukan update data ruangan!']);
                    }                
            }
            
        }
    }
}
