<?php

use App\Http\Controllers\AuthsController;
use App\Http\Controllers\BookingsController;
use App\Http\Controllers\DataPenggunaController;
use App\Http\Controllers\RuanganController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

// Route::get('/', function () { return view('layout/mainbody'); });

Route::get('/', [BookingsController::class, 'pageDashboard'])->name('dashboard');
Route::get('/home', [BookingsController::class, 'pageDashboard'])->name('home');
Route::get('/dashboard', [BookingsController::class, 'pageDashboard'])->name('dashboard');
Route::get('/kalender', [BookingsController::class, 'pageKalender'])->name('kalender');

Route::get('/data-bookingroom', [BookingsController::class, 'pageBookingRoom'])->name('data-bookingroom');
Route::get('/bookingroom-data', [BookingsController::class, 'reqAjaxTableBookingRoom'])->name('bookingroom-data');
Route::get('/data-booked-calendar', [BookingsController::class, 'reqAjaxDataBookingCalendar'])->name('data-booked-calendar');
Route::get('/booking-detail', [BookingsController::class, 'reqajaxDetailBooking'])->name('booking-detail');


Route::get('/login', [AuthsController::class, 'pageLogin'])->name('login');
Route::post('/login-pengguna', [AuthsController::class, 'AjaxprosesLogin']);
Route::get('/logout', [AuthsController::class, 'logout'])->name('logout');
Route::get('/api-bookingrooms', [BookingsController::class, 'reqApiDataBookingRoom']);


// Halaman akses jika user sudah bisa login
Route::middleware(['auth'])->group(function () 
{
    // Data Ruangan

    // Booking Ruangan
    Route::post('/booking-ruangan', [BookingsController::class, 'ajaxFormTambahBooking'])->name('booking-ruangan');
    Route::post('/booking-ruanganedit', [BookingsController::class, 'ajaxFormEditBooking'])->name('booking-ruanganedit');
    Route::get('/hapus-bookingroom', [BookingsController::class, 'reqAjaxHapusBooking'])->name('hapus-bookingroom');

    Route::group(['middleware' => 'checkUserLevel:adm',], function () 
    {
        // Data Users
        Route::get('/data-pengguna', [DataPenggunaController::class, 'pageDataPengguna'])->name('data-pengguna');
        Route::get('/pengguna-data', [DataPenggunaController::class, 'reqAjaxdataPengguna'])->name('pengguna-data');
        Route::get('/pengguna-detail', [DataPenggunaController::class, 'ajaxDetailPengguna'])->name('pengguna-detail');
        Route::post('/edit-pengguna', [DataPenggunaController::class, 'ajaxEditPengguna'])->name('edit-pengguna');
        
        // Data Ruangan
        Route::get('/data-ruangan', [RuanganController::class, 'pageRuangan'])->name('data-ruangan');
        Route::get('/ruangan-data', [RuanganController::class, 'reqAjaxdataRuangan'])->name('ruangan-data');
        Route::post('/tambah-ruangan', [RuanganController::class, 'ajaxTambahRuangan'])->name('tambah-ruangan');
        Route::get('/ruangan-detail', [RuanganController::class, 'ajaxDetailRuangan'])->name('ruangan-detail');
        Route::post('/edit-ruangan', [RuanganController::class, 'ajaxUpdateRuangan'])->name('edit-ruangan');
    });
});