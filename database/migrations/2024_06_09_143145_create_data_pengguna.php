<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('data_pengguna', function (Blueprint $table) {
            $table->char('pgnid', 36)->primary();
            $table->string('email')->unique();
            $table->string('password', 100);
            $table->string('nama_lengkap', 150);
            $table->string('nomor_hp', 20)->nullable();
            $table->string('jabatan', 50)->nullable();            
            $table->enum('level_akun', ['adm', 'pgn']);
            $table->enum('status_akun', ['aktif', 'tidak-aktif'])->nullable();
            $table->string('foto_profile', 80)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('data_pengguna');
    }
};
