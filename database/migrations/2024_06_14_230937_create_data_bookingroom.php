<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('data_bookingroom', function (Blueprint $table) {
            $table->uuid('bookid', 36)->primary();
            $table->char('ruanganid', 36);
            $table->char('penggunaid', 36);
            $table->date('tanggal_mulai');
            $table->date('tanggal_selesai');
            $table->char('waktu_mulai', 5);
            $table->char('waktu_selesai', 5);
            $table->string('tujuan_kegiatanbook', 100);
            $table->text('catatan');
            $table->timestamps();

            $table->foreign('ruanganid')->references('rgnid')->on('data_ruangan')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('penggunaid')->references('pgnid')->on('data_pengguna')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('data_bookingroom');
    }
};
