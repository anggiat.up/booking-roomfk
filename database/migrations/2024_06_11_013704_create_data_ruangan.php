<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('data_ruangan', function (Blueprint $table) {
            $table->uuid('rgnid', 36)->primary();
            $table->string('nama_ruangan', 150);
            $table->string('warna_ruangan', 20);
            $table->enum('status_ruangan', ['tersedia', 'tidak-tersedia', 'maintenance']);
            $table->integer('jumlah_kursi');
            $table->string('keterangan', 50);
            $table->text('fasilitas_lain')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('data_ruangan');
    }
};
