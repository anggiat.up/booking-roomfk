<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Ramsey\Uuid\Uuid;

class DataPenggunaSeeder extends Seeder
{
    public function run(): void
    {
        DB::table('data_pengguna')->insert([
            [
                'pgnid'        => Uuid::uuid4(),
                'email'        => 'admin@gmail.com',
                'password'     => bcrypt('fkur123'),
                'nama_lengkap' => 'Administrator',
                'nomor_hp'     => '081234567890',
                'jabatan'      => 'Jabatan 1',
                'level_akun'   => 'adm',
                'status_akun'  => 'aktif',
                'foto_profile' => 'path/to/foto1.jpg',
                'created_at'   => now(),
                'updated_at'   => now()
            ],
            [
                'pgnid'        => Uuid::uuid4(),
                'email'        => 'member@gmail.com',
                'password'     => bcrypt('fkur123'),
                'nama_lengkap' => 'Hanifa Pratiwi',
                'nomor_hp'     => '081234567891',
                'jabatan'      => 'Jabatan 2',
                'level_akun'   => 'pgn',
                'status_akun'  => 'aktif',
                'foto_profile' => 'path/to/foto2.jpg',
                'created_at'   => now(),
                'updated_at'   => now()
            ]
            // Tambahkan data pengguna lainnya sesuai kebutuhan
        ]);
    }
}
