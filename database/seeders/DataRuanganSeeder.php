<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Ramsey\Uuid\Uuid;

class DataRuanganSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('data_ruangan')->insert([
            [
                'rgnid'          => Uuid::uuid4(),
                'nama_ruangan'   => 'Auditorium Lt.2',
                'warna_ruangan'  => '#FF5733',
                'status_ruangan' => 'tersedia',
                'jumlah_kursi'   => 20,
                'keterangan'     => 'Ruang rapat Auditorium',
                'fasilitas_lain' => 'Proyektor, Whiteboard',
                'created_at'     => now(),
                'updated_at'     => now()
            ],
            [
                'rgnid'          => Uuid::uuid4(),
                'nama_ruangan'   => 'Studio Lt.1',
                'warna_ruangan'  => '#33FF57',
                'status_ruangan' => 'tidak-tersedia',
                'jumlah_kursi'   => 10,
                'keterangan'     => 'Ruang diskusi kecil dengan AC',
                'fasilitas_lain' => 'AC, Wi-Fi',
                'created_at'     => now(),
                'updated_at'     => now()
            ],
            [
                'rgnid'          => Uuid::uuid4(),
                'nama_ruangan'   => 'Senat Lt.5',
                'warna_ruangan'  => '#3357FF',
                'status_ruangan' => 'maintenance',
                'jumlah_kursi'   => 30,
                'keterangan'     => 'Ruang meeting dilantai 5',
                'fasilitas_lain' => 'Komputer, Proyektor',
                'created_at'     => now(),
                'updated_at'     => now()
            ]
        ]);
    }
}
